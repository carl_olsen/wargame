'use strict';
var _      = require('lodash');
var extend = require('./extend');
var mixins = require('./mixins');

var Sprite = extend(Phaser.Sprite, {
    constructor: function Sprite(settings) {
        var defaults = {
            game:     null,
            x:        0,
            y:        0,
            textures: {}
        };

        var s = settings = _.defaultsDeep(settings, defaults);

        Phaser.Sprite.call(this, s.game, s.x, s.y, s.textures.default);
        this.anchor.set(0.5);
        this.textures = s.textures;
    },
    display: function(texture) {
        this.loadTexture(this.textures[texture]);
    },
    update:function(){
        this.updateFollowCoords();
    },
});

_.assign(
    Sprite.prototype,
    mixins.displayObject.follow,
    mixins.displayObject.cascadeDestroy
);

module.exports = Sprite;