'use strict';

var _      = require('lodash');
var extend = require('../extend');
var Sprite = require('../sprite');

var rrange = function(min, max) {
    return Math.random() * (max - min) + min;
};

var BeamGroup = extend(Phaser.Group, {
    constructor: function BeamGroup(settings) {
        var game     = settings.game;
        var defaults = {
            game:    null,
            xMargin: 0,
            time:    200,
            color:   '#ff0000',
            source:  null,
            offset:  null,
        };
        var s = settings = _.defaultsDeep(settings, defaults);

        Phaser.Group.call(this, game);

        this.xMargin = s.xMargin;
        this.color   = s.color;
        this.source  = s.source;
        this.target  = s.target;
        var time = s.time;

        var bmd    = game.add.bitmapData(game.stage.width, game.stage.height);
        var sprite = new Sprite({
            game:     game,
            textures: {
                default: bmd
            }
        });
        this.bitmapData = bmd;
        sprite.anchor.set(0);
        this.add(sprite);

        var tween = game.add.tween(this);
        this.alpha  = 0.25;
        this.driftX = rrange(-10, 10);
        this.driftY = rrange(-10, 10);

        var toState = {
            alpha:  [0.7, 0],
            driftX: rrange(-10, 10),
            driftY: rrange(-10, 10)
        };
        tween.to(toState, time * 2)

        var beam = this;
        tween.onComplete.add(function() {
            beam.destroy();
        });
        tween.start();

        this.tween = tween;
    },

    updateAlignment: function() {
        var a       = this.source;
        var b       = this.target;
        var aOffset = this.sourceOffset;
        var bOffset = this.targetOffset;

        if (!a || !b) {
            return;
        }

        var ax = a.x;
        var ay = a.y;
        var bx = b.x;
        var by = b.y;

        if (aOffset) {
            ax += aOffset.x;
            ay += aOffset.y;
        }

        if (bOffset) {
            bx += bOffset.x;
            by += bOffset.y;
        }

        bx += this.driftX || 0;
        by += this.driftY || 0;

        this.bitmapData.clear();
        this.bitmapData.line(ax, ay, bx, by, this.color, 5);
        this.bitmapData.line(ax, ay, bx, by, '#fff', 1);
        this.bitmapData.dirty = true;

    },

    update: function() {
        this.updateAlignment();
    }

});
module.exports = BeamGroup;
