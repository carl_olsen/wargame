'use strict';
var _      = require('lodash');
var extend = require('../extend');
var Group = require('../group');

var BarGroup = extend(Group, {
    groupType: 'bar',

    constructor: function BarGroup(settings) {
        var game     = settings.game;
        var defaults = {
            game:           null,
            projectileType: null,
            width:          20,
            height:         4,
            bgColor:        '#000',
            border:         2,
            lowColor:       '#ff0000',
            midColor:       '#FFA500',
            highColor:      '#00ff00',
        };

        var s = settings = _.defaultsDeep(settings, defaults);
        Group.call(this, settings);

        var lowColor  = this.lowColor(s.lowColor);
        var midColor  = this.midColor(s.midColor);
        var highColor = this.highColor(s.highColor);

        var bg = game.add.bitmapData(s.width, s.height);
        bg.ctx.fillStyle = s.bgColor;
        bg.fill();

        var bgSprite = game.add.sprite(0, 0, bg);

        var bwidth  = s.width - s.border * 2;
        var bheight = s.height - s.border * 2;
        var bar     = game.add.bitmapData(bwidth, bheight);

        bar.ctx.fillStyle = highColor;
        bar.fill();

        var barSprite = game.add.sprite(s.border, s.border, bar);
        this.add(bgSprite);
        this.add(barSprite);

        this.barWidthFull  = bwidth;
        this.bgSprite      = bgSprite;
        this.barBitmapData = bar;
        this.barSprite     = barSprite;
        this.percent       = 1;
        this.dirty         = true;
    },
    lowColor: function(val) {
        if (val !== undefined) {
            this._lowColor = Phaser.Color.valueToColor(val);
        }
        return this._lowColor;
    },
    midColor: function(val) {
        if (val !== undefined) {
            this._midColor = Phaser.Color.valueToColor(val);
        }
        return this._midColor;
    },
    highColor: function(val) {
        if (val !== undefined) {
            this._highColor = Phaser.Color.valueToColor(val);
        }
        return this._highColor;
    },
    barColor: function(percent) {
        var a, b;

        if (percent >= 0.5) {
            a = this.midColor();
            b = this.highColor();
            percent -= 0.5;
            percent *= 2;

        } else {
            a = this.lowColor();
            b = this.midColor();
            percent *= 2;
        }
        var steps       = 100;
        var currentStep = Math.round(percent * 100);
        var color = Phaser.Color.interpolateRGB(a.r, a.g, a.b, b.r, b.g, b.b, steps, currentStep);

        return Phaser.Color.getWebRGB(color);
    },
    update: function() {
        this.updateFollowCoords();
        if(this.updatePercent){
            this.updatePercent();
        }
        if (this.dirty) {
            this.draw();
        }
    },
    draw: function() {
        var percent = this._percent
        var width   = percent * this.barWidthFull;
        this.barSprite.width             = width;
        this.barBitmapData.ctx.fillStyle = this.barColor(percent);
        this.barBitmapData.fill();
        this.dirty = false;
    }

});

Object.defineProperty(BarGroup.prototype, "percent", {

    get: function() {
        return this._percent;
    },

    set: function(val) {
        if (this._percent !== val) {
            this.dirty = true;
        }
        if(val < 0){
            val = 0;
        }
        this._percent = val;
    }

});

module.exports = BarGroup;
