'use strict';

var _      = require('lodash');
var extend = require('../extend');
var Sprite = require('../sprite');
// var LineSprite = require('../sprite/line');

var range = function(r, min, max) {
    return r * (max - min) + min;
};

var wobble = function(sprite, settings) {

    var defaults = {
        heightRatioMin: 0.5,
        heightRatioMax: 1.5,
        alphaMin:       0.2,
        alphaMax:       1,
        timeMin:        60,
        timeMax:        90
    };

    var s = settings = _.defaults(settings, defaults);

    var tween = sprite.game.add.tween(sprite);

    sprite.events.onDestroy.add(function() {
        tween.stop();
    });

    var queue = function() {
        var r    = Math.random();
        var hMin = s.heightRatioMin;
        var hMax = s.heightRatioMax;
        var aMin = s.alphaMin;
        var aMax = s.alphaMax;
        var tMin = s.timeMin;
        var tMax = s.timeMax;

        var heightAdjustment = range(r, hMin, hMax);
        var alpha            = range(1 - r, aMin, aMax);
        var time             = range(Math.random(), tMin, tMax);

        var toState = {
            height: sprite.baseHeight * heightAdjustment,
            alpha:  alpha
        };

        tween.to(toState, time);
        tween.onComplete.add(queue);
        tween.start();
    };

    // queue();
};


var fade = function(group, sprite, settings) {

    var defaults = {
        heightRatioMin: 1,
        heightRatioMax: 2,
        alphaMin:       0,
        alphaMax:       1,
        time:           200,
    };

    var s = settings = _.defaults(settings, defaults);

    var tween = sprite.game.add.tween(sprite);

    tween.onComplete.add(function() {
        group.destroy();
    });

    var startState = {
        height: s.heightStart,
        alpha:  s.alphaMin,
    };

    sprite.height = startState.height;
    sprite.alpha  = startState.alpha;

    var peakState = {
        height: s.heightEnd,
        alpha:  s.alphaMax,
    // width: sprite.width * 0.5
    };

    var endState = {
        height: startState.height,
        alpha:  0,
    // width: 0,
    };

    tween.to(peakState, s.time * 0.5, Phaser.Easing.Exponential.In);
    tween.to(endState, s.time * 0.5, Phaser.Easing.Exponential.Out);
    tween.start();
};


var Line = function(settings) {
    // var game     = settings.game;
    var defaults = {
        game:     null,
        x:        0,
        y:        0,
        width:    100,
        height:   20,
        textures: {
            default: null,
        },
    };
    var s = settings = _.defaultsDeep(settings, defaults);

    var sprite = new Sprite(settings);

    sprite.baseHeight = s.height;
    sprite.height     = s.height;
    sprite.width      = s.width;

    return sprite;
};

var BeamGroup = extend(Phaser.Group, {
    constructor: function BeamGroup(settings) {
        var game     = settings.game;
        var defaults = {
            game:    null,
            xMargin: 0,
            time:    200,
        };
        var s = settings = _.defaultsDeep(settings, defaults);

        Phaser.Group.call(this, game);

        this.xMargin = settings.xMargin;
        this.line    = new Phaser.Line();

        var bg = Line({
            game:     game,
            height:   10,
            textures: {
                default: game.cache.getBitmapData('beam')
            }
        });

        bg.anchor.set(0, 0.5);

        var inner = Line({
            game:     game,
            height:   2,
            textures: {
                default: game.cache.getBitmapData('beam_inner')
            }
        });
        inner.anchor.set(0, 0.5);


        this.innerLine = inner;
        this.bgLine    = bg;

        this.addChild(this.bgLine);
        this.addChild(this.innerLine);


        // fade(this, bg, {
        //     heightStart: 6,
        //     heightEnd:   10,
        //     alphaMin:    0,
        //     alphaMax:    0.9,
        //     time:        s.time
        // });


        // fade(this, inner, {
        //     heightStart: 4,
        //     heightEnd:   2,
        //     alphaMin:    0,
        //     alphaMax:    1,
        //     time:        s.time
        // });
    },

    alignWith: function(a, b) {
        this.spriteA = a;
        this.spriteB = b;
        var line = this.line.setTo(a.x, a.y, b.x, b.y);

        this.x = a.x;
        this.y = a.y;

        this.width = line.length - this.xMargin * 2;
    },

    update: function() {
        // console.log('up', this.spriteA, this.spriteB)
        if (this.spriteA && this.spriteB) {
            var a = this.spriteA;
            var b = this.spriteB;

            var line = this.line.setTo(a.x, a.y, b.x, b.y);
            // var mid  = line.midPoint();

            this.x = a.x;
            this.y = a.y;

            this.width    = line.length;// - this.xMargin * 2;
            this.rotation = line.angle;

            // console.log(d);
            // this.height = this.baseHeight * d;

        }
    }

});
module.exports = BeamGroup;
