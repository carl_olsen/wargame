'use strict';

Phaser.Events.prototype.afterRemoveAll = function(context){

};

Phaser.Events.prototype.removeAll = function(context){
    if (this._onDestroy)           { this._onDestroy.removeAll(context); }
    if (this._onAddedToGroup)      { this._onAddedToGroup.removeAll(context); }
    if (this._onRemovedFromGroup)  { this._onRemovedFromGroup.removeAll(context); }
    if (this._onRemovedFromWorld)  { this._onRemovedFromWorld.removeAll(context); }
    if (this._onKilled)            { this._onKilled.removeAll(context); }
    if (this._onRevived)           { this._onRevived.removeAll(context); }
    if (this._onEnterBounds)       { this._onEnterBounds.removeAll(context); }
    if (this._onOutOfBounds)       { this._onOutOfBounds.removeAll(context); }

    if (this._onInputOver)         { this._onInputOver.removeAll(context); }
    if (this._onInputOut)          { this._onInputOut.removeAll(context); }
    if (this._onInputDown)         { this._onInputDown.removeAll(context); }
    if (this._onInputUp)           { this._onInputUp.removeAll(context); }
    if (this._onDragStart)         { this._onDragStart.removeAll(context); }
    if (this._onDragUpdate)        { this._onDragUpdate.removeAll(context); }
    if (this._onDragStop)          { this._onDragStop.removeAll(context); }

    if (this._onAnimationStart)    { this._onAnimationStart.removeAll(context); }
    if (this._onAnimationComplete) { this._onAnimationComplete.removeAll(context); }
    if (this._onAnimationLoop)     { this._onAnimationLoop.removeAll(context); }

    this.afterRemoveAll(context);
};



// Object.defineProperty(Phaser.TweenData.prototype, 'value', {

//     get: function() {
//         return this._value;
//         // return  this.position.x;
//     },

//     set: function(value) {
//         if(_.isNaN(value)){
//             throw Error('z');
//         }
//         this._value = value;
//     }

// });

// Object.defineProperty(Phaser.TweenData.prototype, 'dt', {

//     get: function() {
//         return this._dt;
//         // return  this.position.x;
//     },

//     set: function(value) {
//         if(_.isNaN(value)){
//             throw Error('z');
//         }
//         this._dt = value;
//     }

// });