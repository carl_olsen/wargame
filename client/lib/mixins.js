'use strict';

var mixins = {
    displayObject: {
        follow: {
            followOffsetX: 0,
            followOffsetY: 0,
            updateFollowCoords: function() {
                if (!this._followTarget) {
                    return;
                }

                this.x = this._followTarget.x + this.followOffsetX || 0;
                this.y = this._followTarget.y + this.followOffsetY || 0;
            },
            followCoords: function(target, followOffsetX, followOffsetY) {
                if(followOffsetX !== undefined){
                    this.followOffsetX = followOffsetX;
                }
                if(followOffsetY !== undefined){
                    this.followOffsetY = followOffsetY;
                }
                this._followTarget = target;
            },
        },
        cascadeDestroy: {
            cascadeDestroyEventFrom: function(obj) {
                var self = this;
                obj.events.onDestroy.add(function() {
                    self.destroy();
                });
            }
        }
    }
};

module.exports = mixins;