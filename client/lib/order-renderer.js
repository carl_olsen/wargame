'use strict';

var _ = require('lodash');

var OrderRenderer = function OrderRenderer(settings) {
    var defaults = {
        game: null,
    };
    var s = _.merge(defaults, settings);

    var game = s.game;

    var out = {
        bitmap: null,
        sprite: null,

        init: function() {
            this.bitmap = game.add.bitmapData(game.stage.width, game.stage.width, 'lines', true);
            this.sprite = game.add.sprite(0, 0, this.bitmap);

            this.bitmapTop = game.add.bitmapData(game.stage.width, game.stage.width, 'top_lines', true);
            this.spriteTop = game.add.sprite(0, 0, this.bitmapTop);
        },
        update: function(units) {

            var bitmap  = this.bitmap;
            var ctx     = this.bitmap.ctx;
            var fbitmap = this.bitmapTop;
            var fctx    = this.bitmapTop.ctx;

            this.bitmap.clear();
            this.bitmapTop.clear();

            // ctx.strokeStyle = '#9cffc6';
            ctx.strokeStyle = 'rgba(0,0,0,0.7)';
            ctx.lineWidth   = 3;

            fctx.strokeStyle = '#9cffc6';
            fctx.lineWidth   = 1;

            // fctx.fillStyle = '#9cffc6';
            // fctx.fillRect(10, 10, 20, 20);

            units.forEach(function(unit) {


                if (unit) {
                    var orderId = unit.currentOrderId;

                    ctx.beginPath();
                    fctx.beginPath();

                    ctx.moveTo(unit.x, unit.y);
                    fctx.moveTo(unit.x, unit.y);
                    var prev = {
                        x: unit.x,
                        y: unit.y
                    };

                    unit.orders.forEach(function(order) {
                        var index = unit.orders.indexOf(order);

                        var visibleOrder = !orderId || orderId <= index;

                        if(unit.getBounds().contains(order.x, order.y)){
                            visibleOrder = false;
                        }

                        if(!visibleOrder){
                            order.visible = false;
                            order.line.visible = false;
                            return;
                        }
                        order.visible      = true;
                        order.line.visible = true;

                        ctx.lineTo(order.x, order.y);
                        fctx.lineTo(order.x, order.y);
                        var line = new Phaser.Line(prev.x, prev.y, order.x, order.y);
                        var mid  = line.midPoint();
                        order.line.x = mid.x;
                        order.line.y = mid.y;

                        order.line.width    = line.length - 20;
                        order.line.rotation = line.angle;

                        prev = {
                            x: order.x,
                            y: order.y
                        };
                    });

                    ctx.stroke();
                    ctx.closePath();
                    fctx.stroke();
                    fctx.closePath();
                }

            })
            // this.bitmap.render();

            var mask      = game.cache.getImage('order_mask');
            var maskInner = game.cache.getImage('order_mask_inner');

            units.forEach(function(unit) {
                var ux = unit.x - unit.offsetX;
                var uy = unit.y - unit.offsetY;
                // bitmap.draw(mask, ux, uy, null, null, 'destination-out');
                fbitmap.draw(maskInner, ux, uy, null, null, 'destination-out');

                unit.orders.forEach(function(order) {
                    var ox = order.x - order.offsetX;
                    var oy = order.y - order.offsetY;

                    if(order.visible){
                        bitmap.draw(mask, ox, oy, null, null, 'destination-out');
                        fbitmap.draw(maskInner, ox, oy, null, null, 'destination-out');
                    }

                });
            });

            this.bitmapTop.render();
            this.bitmap.render();
        }
    };

    out.init();

    return out;

};
module.exports = OrderRenderer;
