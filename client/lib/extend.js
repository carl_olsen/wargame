'use strict';
var _ = require('lodash');

var extend = function(parentConsturctor, subProto) {

    var c = subProto.constructor;
    c.prototype.constructor = c;
    c.prototype             = _.assign(
        Object.create(parentConsturctor.prototype),
        subProto
    );
    return c;
};
module.exports = extend;