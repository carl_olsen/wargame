'use strict';
var _ = require('lodash');
var extend = require('./extend');

var Tilemap = extend(Phaser.Tilemap, {
    constructor: function Tilemap(settings) {
        var defaults = {
            game:     null,
            key: null,
        };
        var s = _.merge(defaults, settings);
        Phaser.Tilemap.call(this, s.game, s.key);


        this.addTilesetImage('overworld');

        this.layer_base = this.createLayer('base');
        this.layer_overlay_1 = this.createLayer('overlay_1');

        this.layer_base.smoothed = false;
        this.layer_overlay_1.smoothed = false;
    },
    tileData: function(x, y){
        var tile = this.getTile(x, y, this.layer_overlay_1);

        if(!(tile && tile.properties && tile.properties.type)){
            tile = this.getTile(x, y, this.layer_base);
        }

        if(tile){
            return tile.properties;
        }
    },
    setScale: function(val){
        this.layer_base.setScale(val);
        this.layer_overlay_1.setScale(val);
    }

});
module.exports = Tilemap;