'use strict';
var _                  = require('lodash');
var app                = require('../app');
var TickActionAnimator = require('../tick-action-animator');
var c                  = require('../../shared/config');
var stampit            = require('stampit');

var TurnAnimatorData = stampit()
    .init(function(s) {
        // var args = s.args;
        // var instance = s.instance;
        // var stamp = s.stamp;

        this.unitTweens     = this.initUnitTweens(this.units);
        this.unitTickStates = this.initUnitTickStates(this.units, this.tickCount);
        this.mainTween      = this.initMainTween();

    })
    .props({
        tickCount:            1,
        units:                null,
        unitTweens:           {},
        unitTickStates:       {},
        tickCompleteBindings: {},
        mainTween:            null,
        tickAnimationTime:    null,
    })
    .methods({
        initMainTween: function() {
            var tween = this.game.add.tween({});
            for (var i = 0; i < this.tickCount; i++) {
                tween.to({
                    tickId: i
                }, this.tickAnimationTime)
            }

            tween.onChildComplete.add(function(state, tween) {
                var tickId = tween.current - 1;

                var bindings = this.tickCompleteBindings[tickId];
                if (!bindings) {
                    return;
                }

                for (var i = 0; i < bindings.length; i++) {
                    var fn = bindings[i];
                    fn();
                }
            }, this);
            return tween;
        },
        initUnitTweens: function(units) {
            var out  = {};
            var game = this.game;
            _.each(units, function(unit) {
                var unitId = unit.unitId;
                out[unitId] = game.add.tween(unit);
            });
            return out;
        },
        initUnitTickStates: function(units, tickCount) {
            var out = {};
            _.each(units, function(unit) {
                var unitId     = unit.unitId;
                var tickStates = [];
                for (var i = 0; i < tickCount; i++) {
                    tickStates[i] = {
                        tickId: i
                    };
                }
                out[unitId] = tickStates;
            });
            return out;
        },
        getUnitTween: function(unitId) {
            return this.unitTweens[unitId];
        },
        getUnitTickState: function(unitId, tickId) {
            return this.unitTickStates[unitId][tickId];
        },
        assignUnitTickState: function(unitId, tickId, obj){
            var tickState = this.getUnitTickState(unitId, tickId);
            _.assign(tickState, obj);
        },
        addOnTick: function(tickId, fn) {
            if (!this.tickCompleteBindings[tickId]) {
                this.tickCompleteBindings[tickId] = [];
            }
            this.tickCompleteBindings[tickId].push(fn);
        },
        queueTickStateTweens: function() {
            var data              = this;
            var tickAnimationTime = this.tickAnimationTime;
            _.each(this.unitTickStates, function(tickStates, unitId) {
                var tween = data.getUnitTween(unitId);
                _.each(tickStates, function(tickState, tickId) {
                    tween.to(tickState, tickAnimationTime)
                });
            });
        },
        startTweens: function() {
            this.mainTween.start();
            _.each(this.unitTweens, function(tween, unitId) {
                // console.log('ss', tween.generateData());
                tween.start();
            });
        }
    });

var TurnAnimator = stampit()
    .init(function() {

        this.timer = this.game.time.create();

        this.turnAnimatorData = TurnAnimatorData({
            game:              this.game,
            tickCount:         c.ticks_per_turn,
            units:             app.units,
            tickAnimationTime: this.tickAnimationTime,
        });

    // this.unitTickActions = this.initUnitTickActions();
    })
    .props({
        game:              null,
        tickAnimationTime: 200,
    })
    .methods({
        // initUnitTickActions: function() {
        //     var unitTickActions = [];

        //     _.each(this.ticks, function(tickActions, tickId) {
        //         tickActions.forEach(function(tickAction) {

        //             var unitId = tickAction.unitId;

        //             if (!unitTickActions[unitId]) {
        //                 unitTickActions[unitId] = [];
        //             }

        //             if (!unitTickActions[unitId][tickId]) {
        //                 unitTickActions[unitId][tickId] = [];
        //             }
        //             !unitTickActions[unitId][tickId].push(tickAction);
        //         });
        //     });
        //     return unitTickActions;
        // },
        validate: function() {
            var unitIds      = _.map(app.units, 'unitId');
            var tickIdErrors = [];

            var missing = [];
            _.each(this.ticks, function(tick, tickId) {
                var unitIdList = [].concat(unitIds);

                tick.forEach(function(tickAction) {
                    var unitId = tickAction.unitId;

                    if (tickId !== tickAction.tickId) {
                        tickIdErrors.push({
                            tickId:     tickId,
                            tickAction: tickAction
                        });
                    }

                    var index = unitIdList.indexOf(unitId);
                    if (index !== -1) {
                        unitIdList.splice(index, 1);
                    }
                });

                if (unitIdList.length) {
                    missing.push({
                        tickId:                  tickId,
                        unitsWithoutTickActions: unitIdList
                    });
                }

            });

            if (missing.length) {
                console.log('missing unit tick actions', missing);
                console.error('missing unit tick actions', missing);
            }

            if (tickIdErrors.length) {
                console.log('tickIdErrors unit tick actions', tickIdErrors);
                console.error('tickIdErrors unit tick actions', tickIdErrors);
            }

        },

        queue: function() {
            var game              = this.game;
            var tickAnimationTime = this.tickAnimationTime;

            var turnAnimatorData = this.turnAnimatorData;

            _.each(this.turnResults, function(unitTicks, unitId) {

                _.each(unitTicks, function(unitTick, tickId) {

                    turnAnimatorData.assignUnitTickState(unitId, tickId, unitTick.unitState);

                    _.each(unitTick.tickActions, function(tickAction) {

                        var type = tickAction.tickActionType;

                        var TickAnimator = TickActionAnimator(type);

                        var animator = TickAnimator({
                            game: game,

                            tickId:            tickId,
                            tickAction:        tickAction,
                            tickAnimationTime: tickAnimationTime,
                            turnAnimatorData:  turnAnimatorData,
                        });

                        animator.queue();
                    });
                });
            });
            var _this = this;

            turnAnimatorData.queueTickStateTweens();
            turnAnimatorData.mainTween.onComplete.add(function() {
                _this.timer.pause();
            });
        },


        play: function() {

            _.each(app.units, function(unitSprite) {
                unitSprite.currentOrderId = 0;
            });
            this.timer.start();
            this.turnAnimatorData.startTweens();

        }
    });


module.exports = TurnAnimator;