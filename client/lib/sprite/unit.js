'use strict';
var _      = require('lodash');
var extend = require('../extend');
var Sprite = require('../sprite');
var Bar    = require('../group/bar');

var RangeCircle = function(unitSprite) {
    var game = unitSprite.game;

    var maxRange = unitSprite.unitState.weaponMaxRange();
    var padding  = 6;
    var radius   = tileSize * maxRange;
    var width    = radius * 2 + padding;
    var height   = radius * 2 + padding;
    var x        = width * 0.5;
    var y        = height * 0.5;

    var bmd = game.add.bitmapData(width, height);
    bmd.ctx.fillStyle = 'rgba(0,0,0,0)';
    bmd.circle(x, y, radius);
    bmd.ctx.strokeStyle = 'white';
    bmd.ctx.stroke();

    var sprite = new Sprite({
        game:     game,
        textures: {
            default: bmd
        }
    });

    sprite.alpha = 0.15;
    sprite.followCoords(unitSprite);
    sprite.cascadeDestroyEventFrom(unitSprite);

    return sprite;
};

var UnitSprite = extend(Sprite, {
    spriteType: 'unit',

    orders: null, // []

    constructor: function UnitSprite(settings) {
        var defaults = {
            unitId:  null,
            ownerId: null,

            textures: {
                default:     'unit',
                selected:    'unit_selected',
                highlighted: 'unit_highlighted',
            },
        };
        var s = settings = _.defaultsDeep(settings, defaults);

        Sprite.call(this, settings);
        this.inputEnabled             = true;
        this.orders                   = [];
        this.unitId                   = s.unitId;
        this.ownerId                  = s.ownerId;
        this.range                    = s.range;
        this.unitState                = s.unitState;
        this.hp                       = s.hp;
        this.hpMax                    = s.hpMax;
        this.weaponPrimaryCooldownMax = s.weaponPrimaryCooldownMax;
        this.unitType                 = s.unitType;

        var unit = this;
        this.events.afterRemoveAll = function(context) {
            unit.orders.forEach(function(order) {
                order.events.removeAll(context);
            });
        };

        var game = this.game;

        var barFollowOffsetX = -this.width * 0.5;

        this.hpBar = (function() {
            var hpBar = new Bar({
                game:      game,
                width:     this.width,
                height:    5,
                border:    1,
                lowColor:  '#ff0000',
                midColor:  '#FFA500',
                highColor: '#00ff00',

            });
            var followOffsetX = barFollowOffsetX;
            var followOffsetY = -16;

            hpBar.followCoords(this, followOffsetX, followOffsetY);
            hpBar.cascadeDestroyEventFrom(this);
            hpBar.updatePercent = function() {
                this.percent = unit.hp / unit.hpMax;
            };
            return hpBar;

        }).call(this);

        this.cooldownBar = (function() {
            var cooldownBar = new Bar({
                game:           game,
                width:          this.width,
                height:         4,
                border:         1,
                lowColor:       '#fff',
                midColor:       '#808080',
                highColor:      '#00aeef',
                inversePercent: true,
            });

            var followOffsetX = barFollowOffsetX;
            var followOffsetY = -22;

            cooldownBar.followCoords(this, followOffsetX, followOffsetY);
            cooldownBar.cascadeDestroyEventFrom(this);
            cooldownBar.updatePercent = function() {
                this.percent = 1 - (unit['weapons.primary.cooldownRemaining'] / unit.weaponPrimaryCooldownMax);
            };

            return cooldownBar;

        }).call(this);

        (function() {
            // var style = {
            //     font:          "12px verdana",
            //     fill:          "#fff",
            //     wordWrap:      true,
            //     wordWrapWidth: this.width,
            //     align:         "center"
            // };

            var char = this.unitId || 'x';
            var y = 0;
            if (this.unitType == 'tank') {
                char = 'T';
                y = 0;
            }
            if (this.unitType == 'scout') {
                char = 's';
                y = -2;
            }
            var text = this.game.add.bitmapText(0, y, 'gem', char, 16);

            // var text = this.game.add.text(0, 2, char, style);

            text.anchor.set(0.5);
            this.addChild(text);
            this.text = text;
        }).call(this);

        this.rangeCircle = RangeCircle(this);
    },

    addOrder: function(order, insertIndex) {
        var index = this.orders.indexOf(order);
        if (index !== -1) {
            return;
        }

        order.unitSprite = this;

        order.events.onDestroy.add(function() {
            this.removeOrder(order);
        }, this);

        this.events.onDestroy.add(function() {
            order.destroy();
        });

        if (insertIndex !== undefined) {
            this.orders.splice(insertIndex, 0, order);
        } else {
            this.orders.push(order);
        }

    },
    removeOrder: function(order) {
        var index = this.orders.indexOf(order);
        if (index !== -1) {
            this.orders.splice(index, 1);
        }
    },
    ordersDisplay: function(val) {
        this.orders.forEach(function(order) {
            order.display(val);
        });
    },
    getSelectedOrderFallback: function(order) {
        var index = this.orders.indexOf(order);
        var nextSelected;
        if (this.orders.length === 1) {
            nextSelected = this;
        } else if (index === 0) {
            // select next if first
            nextSelected = this.orders[index + 1];
        } else {
            // select prev
            nextSelected = this.orders[index - 1];
        }
        return nextSelected;
    },
    ordersToJSON: function() {
        return this.orders.map(function(order) {
            return order.toJSON();
        });
    },
    update: function() {
        if (this.hp <= 0) {
            this.alpha               = 0.3;
            this.hpBar.visible       = false;
            this.cooldownBar.visible = false;
        }
    },

    selected: function(val) {
        if (val) {
            this.display('selected');
            this.ordersDisplay('highlighted');
        } else {
            this.display('default');
            this.ordersDisplay('default');
        }
    },
});

module.exports = UnitSprite;
