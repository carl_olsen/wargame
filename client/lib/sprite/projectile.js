'use strict';

var _      = require('lodash');
var extend = require('../extend');
var Sprite = require('../sprite');

var ProjectileSprite = extend(Sprite, {
    spriteType: 'bullet',

    constructor: function ProjectileSprite(settings) {
        var game = settings.game;
        var defaults = {
            game:       null,
            projectileType:  null,
            textures:   {
                default: null,
            }
        };


        var s = settings = _.defaultsDeep(settings, defaults);
        settings.textures.default = game.cache.getBitmapData('particle');
        Sprite.call(this, settings);

    },
});

module.exports = ProjectileSprite;