'use strict';

var _      = require('lodash');
var extend = require('../extend');
var Sprite = require('../sprite');

var OrderLineSprite = extend(Sprite, {
    constructor: function OrderLineSprite(settings) {
        var defaults = {
            order:      null,
            selectable: false,
            textures:   {
                default: 'green_unit',
            },
        };
        settings = _.merge(defaults, settings);
        Sprite.call(this, settings);
        this.order                   = settings.order;
        this.width                   = 100;
        this.height                  = 20;
        this.alpha                   = 0.5;
        this.inputEnabled            = true;
        this.input.pixelPerfectClick = true;
        this.input.pixelPerfectOver  = true;
        this.input.useHandCursor     = true;
        this.alpha                   = 0;
        this.tint                    = 0xff0000;
    }
});

var OrderSprite = extend(Sprite, {
    spriteType: 'order',

    constructor: function OrderSprite(settings) {
        var defaults = {
            game:       null,
            orderType:  'move',
            unitSprite: null,
            dragBounds: null,
            textures:   {
                default:     'order',
                selected:    'order_selected',
                highlighted: 'order_highlighted',
            },
        };
        var s = settings = _.defaultsDeep(settings, defaults);

        Sprite.call(this, settings);
        this.unitSprite = s.unitSprite;
        this.orderType = s.orderType,
        this.inputEnabled = true;
        this.input.enableDrag();
        this.input.boundsRect = s.dragBounds;
        // this.input.enableSnap(tileSize, tileSize);

        this.line = new OrderLineSprite({
            game:  this.game,
            order: this,
        });

        this.game.add.existing(this.line);

        var _this = this;

        this.events.afterRemoveAll = function(context) {
            _this.line.events.removeAll(context);
        };

        this.events.onDestroy.add(function() {
            this.line.destroy();
        }, this);


    },
    toJSON: function() {
        return {
            orderType: this.orderType,
            dest:      {
                x: this.x,
                y: this.y,
            },
        };
    },
    selected: function(val) {

        var unitSprite = this.unitSprite;

        if (val) {
            unitSprite.display('highlighted');
            unitSprite.ordersDisplay('highlighted');
            this.display('selected');
            unitSprite.rangeCircle.followCoords(this);
        } else {
            unitSprite.display('default');
            unitSprite.ordersDisplay('default');
            this.display('default');
            unitSprite.rangeCircle.followCoords(unitSprite);
        }
    }
});

module.exports = OrderSprite;