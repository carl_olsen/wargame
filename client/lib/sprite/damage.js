'use strict';

var Damage = function(settings) {

    var defaults = {
        target: null,
        time:   800,
        damage: 1,
        tint:   0xcc1e24,
        startX: 0,
        startY: -20,
        endX:   0,
        endY:   -40,
        game: null,
    };
    var s      = settings = _.defaults(settings, defaults);
    var damage = s.damage + '';

    var sx = s.startX + s.target.x;
    var sy = s.startY + s.target.y;
    var ex = s.endX + s.target.x;
    var ey = s.endY + s.target.y;

    var text = s.game.add.bitmapText(sx, sy, 'gem', damage, 16);

    text.anchor.x = 0.5;
    text.anchor.y = 0.5;
    text.tint     = s.tint;

    var toState = {
        x:     ex,
        y:     ey,
        alpha: 0.5,
    };
    var tween = s.game.add.tween(text).to(toState, s.time).start();

    tween.onComplete.add(function() {
        text.destroy();
    });

    return text;
};
module.exports = Damage ;