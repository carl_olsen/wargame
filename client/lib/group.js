'use strict';
var _      = require('lodash');
var extend = require('./extend');
var mixins = require('./mixins');

var Group = extend(Phaser.Group, {
    constructor: function Group(settings) {
        var defaults = {
            game:            undefined,
            parent:          undefined,
            name:            undefined,
            addToStage:      undefined,
            enableBody:      undefined,
            physicsBodyType: undefined,
        };

        var s = settings = _.defaults(settings, defaults);

        Phaser.Group.call(this, s.game, s.parent, s.name, s.addToStage, s.enableBody, s.physicsBodyType);
        // var props = _.omit(settings, _.keys(defaults));
        // _.assign(this, props);
    },

    update: function() {
        this.updateFollowCoords();
    },
});

_.assign(
    Group.prototype,
    mixins.displayObject.follow,
    mixins.displayObject.cascadeDestroy
);

module.exports = Group;