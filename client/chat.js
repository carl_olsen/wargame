'use strict';

var Chat = stampit()
    .props({
        // required
        primus:        null,
        primusMessage: null,
        form:          null,
        log:           null,
        inputMessage:  null,
        inputUsername: null,
    })
    .init(function(settings) {
        this.form.on('submit', this.submit.bind(this));
        this.primusMessage.onReceive('chat', this.receive, this);
    })
    .methods({
        submit: function(e) {
            if (e && e.preventDefault) {
                e.preventDefault();
            }

            var username = this.inputUsername.val();
            var message  = this.inputMessage.val();

            this.send(username, message);
            this.inputMessage.val('');
        },
        send: function(username, message) {

            this.primus.write({
                type:     'chat',
                username: username,
                message:  message
            });
        },
        receive: function(data) {
            console.log('receive', data);
            var now  = new Date();
            var time = now.getHours() + ':' + now.getMinutes();

            time = '<span class="time">[' + time + ']</span>';
            var user = ' <span class="username">' + data.username + '</span>';

            var msgHtml = '<p class="msg">' + time + user + ': ' + data.message + '</p>';
            this.log.append(msgHtml);
        },
    });

var Rooms = stampit()
    .props({
        // required
        primus:        null,

        $btnRefresh:   null,
        $container:    null,
    })

    .init(function(settings) {
        this.$btnRefresh.on('click', this.refresh.bind(this));

        this.primus.on('data', function(data) {
            if (data.type === 'list_rooms') {
                this.receive(data);
            }
        }, this);
    })

    .methods({

        refresh: function() {
            this.primus.write({
                type: 'list_rooms'
            });
        },

        receive: function(data) {
            console.log('receive', data);

            var rooms = data.rooms;

            var list = '';
            rooms.forEach(function(room){
                list += '<p>' + room + '</p>';
            });

            this.$container.empty().append(list);
        },
    });




$(function() {

    var primus = new Primus();

    var primusMessage = PrimusMessage(primus);

    var $form          = $('#chat-container');
    var $log           = $form.find('.log');
    var $inputMessage  = $form.find('.input-message');
    var $inputUsername = $('.input-username');

    var chat = Chat({
        primus:        primus,
        primusMessage: primusMessage,
        form:          $form,
        log:           $log,
        inputMessage:  $inputMessage,
        inputUsername: $inputUsername
    });


    var rooms = Rooms({
        primus:        primus,
        $btnRefresh: $('.btn-refresh'),
        $container: $('.room-list')
    })

}());

