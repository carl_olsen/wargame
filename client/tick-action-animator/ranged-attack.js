'use strict';
var Beam = require('../lib/group/beam');
var app  = require('../app');

var TickActionAnimator = require('./base');

var RangedAttack = TickActionAnimator
    .compose()
    .props({
        gameState: null,

        game:              null,
        tickId:            null,
        unitSprite:        null,
        tickAction:        null,
        tickAnimationTime: null,

        unitId:       null,
        targetUnitId: null,
    })
    .init(function() {
        this.unitId       = this.tickAction.unitId;
        this.targetUnitId = this.tickAction.targetUnitId;
    })
    .methods({
        queue: function() {
            var unitId = this.unitId;

            var tickState = this.turnAnimatorData.getUnitTickState(unitId, this.tickId);
            this.setSpriteTickState(tickState);
            var tween = this.turnAnimatorData.getUnitTween(unitId);
            this.setBindings(tween);
        },
        setSpriteTickState: function(obj) {
            _.assign(obj, {
                tickId:         this.tickId,
                currentOrderId: this.tickAction.orderId,
            });

            return obj;

        },

        setBindings: function(tween) {

            var game              = this.game;
            var tickAnimationTime = this.tickAnimationTime;
            var targetUnitId      = this.targetUnitId;

            var unitSprite = this.getUnitSprite(this.unitId);

            this.addTickEvent(tween, function() {

                var target = app.getUnitById(targetUnitId);

                var color;

                if (unitSprite.ownerId === 'player1') {
                    color = 'green';
                } else {
                    color = 'red';
                }

                var beam = new Beam({
                    game:   game,
                    time:   tickAnimationTime,
                    source: unitSprite,
                    target: target,
                    color:  color
                });
                game.groups.attack.add(beam);
            });
        },
    });

module.exports = RangedAttack;
