'use strict';
var ProjectileSprite = require('../lib/sprite/projectile');

var TickActionAnimator = require('./base');

// represnets state change
var Move = TickActionAnimator
    .compose()
    .props({
        // Unit
        unitId:    null,
        gameState: null,

        game:              null,
        tickId:            null,
        unitSprite:        null,
        tickAction:        null,
        tickAnimationTime: null,

    })
    .methods({
        queue: function(){
            var tickState = this.turnAnimatorData.getUnitTickState(this.tickAction.unitId, this.tickId);
            this.setSpriteTickState(tickState);

            var game = this.game;
            var dest = this.tickAction.dest;
            this.turnAnimatorData.addOnTick(this.tickId, function(){
                 var projectile = new ProjectileSprite({
                    game: game,
                });
                // projectile.visible = false;
                game.groups.debugBottom.add(projectile);

                projectile.x = dest.x * tileSize;
                projectile.y = dest.y * tileSize;
            });
        },
        setSpriteTickState: function(obj) {

            _.assign(obj, {
                tickId:         this.tickId,
                x:              this.tickAction.dest.x * tileSize,
                y:              this.tickAction.dest.y * tileSize,
                currentOrderId: this.tickAction.orderId,
            });
        },

    });

module.exports = Move;