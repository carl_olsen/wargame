'use strict';

var stampit = require('stampit');
var app = require('../app');
// represnets state change
var TickActionAnimator = stampit()
    .props({

        gameState: null,

        game: null,

        tickId:            null,
        tickAction:        null,
        tickAnimationTime: null,

        turnAnimatorData: null,

        // the unit this tick action is attached to
        unitId: null,
    })
    .methods({
        queue: function(){
            var tickState = this.turnAnimatorData.getUnitTickState(this.tickAction.unitId, this.tickId);
            this.setSpriteTickState(tickState);
        },
        getUnitSprite: function(id){
            return app.getUnitById(id);
        },
        setSpriteTickState: function(obj) {
            _.assign(obj, {
                tickId:         this.tickId,
                currentOrderId: this.tickAction.orderId,
            });

            return obj;
        },
        addTickEvent: function(tween, fn) {
            var tickId  = this.tickId;
            var binding = tween.onChildComplete.add(function(unitSprite, tween) {
                var tweenTickId = tween.current;
                if (tweenTickId !== tickId + 1) {
                    return;
                }
                fn();
                binding.detach();
            });
        },
        setBindings: function(tween) {},
    });

module.exports = TickActionAnimator;
