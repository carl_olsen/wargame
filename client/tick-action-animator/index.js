'use strict';

var types = {
    move: require('./move.js'),
    ranged_attack: require('./ranged-attack.js'),
    standby: require('./standby.js'),
    damage: require('./damage.js'),

};

var TickActionAnimator = function(type) {
    return types[type];
};

module.exports = TickActionAnimator;