'use strict';
var app  = require('../app');

var TickActionAnimator = require('./base');



var Damage = TickActionAnimator
    .compose()
    .props({
        gameState: null,

        game:              null,
        tickId:            null,
        unitSprite:        null,
        tickAction:        null,
        tickAnimationTime: null,

        unitId:       null,
        sourceUnitId: null,
    })
    .init(function() {
        this.unitId       = this.tickAction.unitId;
        this.targetUnitId = this.tickAction.targetUnitId;
    })
    .methods({
        queue: function() {
            var unitId = this.unitId;

            var tween = this.turnAnimatorData.getUnitTween(unitId);
            this.setBindings(tween);
        },
        setBindings: function(tween) {

            var game              = this.game;
            var tickAnimationTime = this.tickAnimationTime;
            var unitId = this.unitId;
            var damage       = this.tickAction.damage;
            this.addTickEvent(tween, function() {

                var target = app.getUnitById(unitId);

                // var animTime = 200;
                // game.time.events.add(animTime, function() {
                    app.addDamage(target, damage);
                // });
            });
        },
    });

module.exports = Damage;
