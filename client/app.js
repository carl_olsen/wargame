'use strict';

var UnitSprite   = require('./lib/sprite/unit');
var OrderSprite  = require('./lib/sprite/order');
var DamageSprite = require('./lib/sprite/damage');

var UnitTickResolver = require('../shared/unit-tick-resolver');
var getTurnResults   = require('../shared/rules/turn');
var c                = require('../shared/config');

var app = {
    game:             null,
    gameState:        null,
    mapName:          null,
    dragBounds:       null,
    keys:             null,
    marker:           null,
    mapSprite:        null,
    units:            [],
    playerId:         'player1',
    selectedUnitText: null,
    init:             function(game) {
        this.game = game;


        // var text = this.game.add.bitmapText(0, -20, 'gem', damage, 16);


    },
    unitToSprite: function(unit) {
        var coord = this.tileToPixelCoords(unit.x, unit.y);

        var textures;
        if (unit.ownerId === this.playerId) {
            textures = {
                default:     'green_unit',
                selected:    'green_unit_selected',
                highlighted: 'green_unit_highlighted',
            };
        } else {
            textures = {
                default:     'red_unit',
                selected:    'red_unit_selected',
                highlighted: 'red_unit_highlighted',
            };
        }

        var sprite = this.addUnitSprite({
            unitId:                   unit.id,
            unitType:                 unit.unitType,
            ownerId:                  unit.ownerId,
            name:                     unit.name,
            unitState:                unit,
            x:                        coord.x,
            y:                        coord.y,
            range:                    unit.range,
            hp:                       unit.hp,
            hpMax:                    unit.hpMax,
            weaponPrimaryCooldownMax: unit.weapons.primary.cooldown,
            textures:                 textures,

        });
        sprite.smoothed = false;

    // if(sprite.ownerId === this.playerId){
    //     sprite.tint = 0x007ece;
    // } else {
    //     sprite.tint = 0xff0000;
    // }
    },
    setTileSize: function(){
        var tileToPixelCoords = this.tileToPixelCoords;
        this.gameState.units.each(function(unit){
            var coord = tileToPixelCoords(unit.x, unit.y);


        });
    },
    loadUnitsFromGameState: function() {
        var _this = this;
        this.gameState.units.each(function(ent, id) {
            _this.unitToSprite(ent);
        });
    },
    pixelToTileCoords: function(px, py) {
        return {
            x: px / tileSize,
            y: py / tileSize
        };
    },

    tileToPixelCoords: function(tx, ty) {
        return {
            x: tx * tileSize,
            y: ty * tileSize,
        }
    },
    addOrder: function(settings) {
        settings = _.defaults(settings, {
            game:       this.game,
            dragBounds: app.dragBounds,
        });

        var order = new OrderSprite(settings);
        this.game.groups.order.add(order);
        return order;
    },
    addUnitSprite: function(settings) {
        settings = _.defaults(settings, {
            game: this.game,
        });

        var unitSprite = new UnitSprite(settings);
        this.game.groups.unit.add(unitSprite);
        this.game.groups.unitUI.add(unitSprite.hpBar);
        this.game.groups.unitUI.add(unitSprite.cooldownBar);
        this.game.groups.unitUI.add(unitSprite.rangeCircle);

        this.units.push(unitSprite);

        return unitSprite;
    },
    getUnitById: function(id) {
        return _.find(this.units, function(unit) {
            return unit.unitId == id;
        });
    },

    sendTurnOrders: function(turnOrdersJson) {
        // console.log('turnOrdersJson', turnOrdersJson);
        var gameState = this.gameState;
        // mock server
        var turnOrders = turnOrdersJson.map(function(eTurnJson) {
            eTurnJson.gameState = gameState;
            return UnitTickResolver.fromJSON(eTurnJson);
        });

        var ticksPerTurn = c.ticks_per_turn;
        var results      = getTurnResults(gameState, turnOrders, [], ticksPerTurn);

        // console.log('results', results);
        this.game.state.start('animate-turn', false, false, results);

    },
    initSelectedUnitText: function() {
        var style = {
            font:     "12px Courier",
            fill:     "#fff",
            wordWrap: true,
        };

        var text = this.game.add.text(10, 522, '', style);
        text.setShadow(0, 1);
        this.selectedUnitText = text;
        var wText = this.game.add.text(200, 522, '', style);
        wText.setShadow(0, 1);
        this.selectedUnitWeaponText = wText;
    },
    setSelectedUnitText: function(unitSprite) {
        console.log('selected', unitSprite);
        // var str = [
        //     'name:  ' + unitSprite.unitState.name,
        //     'speed: ' + unitSprite.unitState.speed,
        //     'hp:    ' + unitSprite.hp + '/' + unitSprite.hpMax
        // ].join('\n');

        // this.selectedUnitText.setText(str);

        // var arr = [];
        // var unit = this.gameState.units.get(unitSprite.unitId);

        // _.each(unit.weapons, function(weapon, key){
        //     arr.push(key + ': ' + weapon.name);
        //     arr.push('  range: ' + weapon.range);
        //     arr.push('cooldown: ' + unit.cooldownRemaining + '/' + weapon.cooldown);
        // });
        // var str = arr.join('\n');

    // this.selectedUnitWeaponText.setText(str);
    },
    addDamage: function(target, amount) {
        var damage = DamageSprite({
            game:   this.game,
            target: target,
            damage: amount,
        // endX: rrange(-20, 20),
        // endY: rrange(-20, 20)
        });
        this.game.groups.damage.add(damage);
    }
};


module.exports = app;
