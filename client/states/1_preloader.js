'use strict';
var app           = require('../app');
var Map           = require('../../shared/map');

var preloaderState = {
    app: app,

    preload: function() {

        var game = this.game;


        // game.load.bitmapFont('carrier_command', 'assets/fonts/carrier_command.png', 'assets/fonts/carrier_command.xml');
        // game.load.bitmapFont('nokia16', 'assets/fonts/nokia16.png', 'assets/fonts/nokia16.xml');
        game.load.bitmapFont('gem', 'assets/fonts/gem.png', 'assets/fonts/gem.xml');

        game.load.image('green_unit', 'img/green_unit.png');
        game.load.image('green_unit_selected', 'img/green_unit_selected.png');
        game.load.image('green_unit_highlighted', 'img/green_unit_highlighted.png');

        game.load.image('red_unit', 'img/red_unit.png');
        game.load.image('red_unit_selected', 'img/red_unit_selected.png');
        game.load.image('red_unit_highlighted', 'img/red_unit_highlighted.png');

        game.load.image('order', 'img/order.png');
        game.load.image('order_mask', 'img/order_mask.png');
        game.load.image('order_mask_inner', 'img/order_mask_inner.png');

        game.load.image('order_selected', 'img/order_selected.png');
        game.load.image('order_highlighted', 'img/order_highlighted.png');

        var mapName = app.mapName;

        game.load.json(mapName + ':map_data', '/map/json/' + mapName + '.json');
        game.load.image(mapName + ':map_img', '/map/img/' + mapName + '.png');

        var b = game.add.bitmapData(4, 4, 'particle', true);

        // draw to the canvas context like normal
        b.ctx.fillStyle = 'yellow';
        b.ctx.fillRect(0, 0, 4, 4);
        b.ctx.strokeStyle = 'black';
        b.ctx.lineWidth   = 2;
        b.ctx.strokeRect(0, 0, 4, 4);


        // beam.ctx.fillStyle = 'orange';
        // beam.ctx.fillRect(0, 0, 4, 1);
        // beam.ctx.fillRect(0, 3, 4, 1);


    },
    create: function() {
        var mapName = app.mapName;


        app.gameState.map = Map({
            data: this.game.cache.getJSON(mapName + ':map_data'),
        });


        this.game.state.start('init', false);
    },
};

module.exports = preloaderState;