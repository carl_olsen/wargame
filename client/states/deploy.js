'use strict';
var app = require('../app');

var deployState = {
    app: app,
};

module.exports = deployState;