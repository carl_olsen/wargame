'use strict';
var app           = require('../app');
var OrderRenderer = require('../lib/order-renderer');

var initKeys = function(game, keys) {
    _.each(keys, function(binding, key) {
        // binding = Phaser.Keyboard.Z
        keys[key] = game.input.keyboard.addKey(binding);
    });
    return keys;
};

var initDragBounds = function(game, margin) {
    return new Phaser.Rectangle(
        margin,
        margin,
        game.width - margin * 2,
        game.height - margin * 2
    );
};

var initMarker = function(game, map, tileSize, scale) {
    var marker    = game.add.graphics();
    var lineWidth = 1;
    marker.lineStyle(lineWidth, 0xffffff, 1);
    marker.drawRect(0, 0, tileSize * scale - lineWidth, tileSize * scale - lineWidth);
    game.input.addMoveCallback(function() {
        var mx = game.input.activePointer.worldX;
        var my = game.input.activePointer.worldY;
        var x  = Math.floor(mx / tileSize) * tileSize;
        var y  = Math.floor(my / tileSize) * tileSize;
        marker.x = x;
        marker.y = y;
    }, this);

    game.input.onDown.add(function() {
        var mx    = game.input.activePointer.worldX;
        var my    = game.input.activePointer.worldY;
        var x     = Math.floor(mx / tileSize);
        var y     = Math.floor(my / tileSize);
        var props = map.getTile(x, y);

        console.log('tile', x, y, props);
    }, this);

    return marker;
};

var initMapSprite = function(game) {
    var mapSprite = game.add.sprite(0, 0, app.mapName + ':map_img');
    mapSprite.smoothed = false;
    mapSprite.scale    = {
        x: tileSize,
        y: tileSize
    };
    mapSprite.inputEnabled = true;
    return mapSprite;
};

var initState = {
    app:    app,
    create: function() {

        var game = this.game;

        game.stage.backgroundColor = '#2d2d2d';

        app.keys = initKeys(game, {
            delete:         Phaser.Keyboard.Z,
            clear_selected: Phaser.Keyboard.C,
            end_turn:       Phaser.Keyboard.E
        });

        var dragBoundsMargin = 40;
        app.dragBounds = initDragBounds(game, dragBoundsMargin);
        app.marker     = initMarker(game, app.map, tileSize, 1);
        app.mapSprite  = initMapSprite(game);

        app.orderRenderer = OrderRenderer({
            game: this.game
        });

        game.groups = {
            debugBottom: game.add.group(game.world, 'debug_bottom'),
            orderBg:     game.add.group(game.world, 'order_bg_group'),
            order:       game.add.group(game.world, 'order_group'),
            unit:        game.add.group(game.world, 'unit_group'),
            attack:      game.add.group(game.world, 'attack_group'),
            unitUI:      game.add.group(game.world, 'unit_ui_group'),
            damage:      game.add.group(game.world, 'damage_group'),
        };

        game.groups.orderBg.add(app.orderRenderer.sprite);
        game.groups.order.add(app.orderRenderer.spriteTop);

        app.loadUnitsFromGameState();

        app.mapName = 'test';

        app.initSelectedUnitText();

        this.game.state.start('set-orders', false);
    },

};


module.exports = initState;