'use strict';
var app = require('../app');

var GameState = require('../../shared/game-state');
var Unit      = require('../../shared/unit');

// mock getting from server
var initGameState = function() {

    var gameState = GameState();

    var unitA = Unit({
        unitType: 'tank',
        ownerId:  'player1',
        x:        20,
        y:        90,
    });

    var unitB = Unit({
        unitType: 'scout',
        ownerId:  'player1',
        x:        80,
        y:        90,
    });

     var unitC = Unit({
        unitType: 'scout',
        ownerId:  'player1',
        x:        65,
        y:        80,
    });
    gameState.units.add(unitA);
    gameState.units.add(unitB);
    gameState.units.add(unitC);


    var unit3 = Unit({
        unitType: 'scout',
        ownerId:  'player2',
        x:        18,
        y:        50,
    });

    var unit4 = Unit({
        unitType: 'scout',
        ownerId:  'player2',
        x:        22,
        y:        50,
    });


    var unit5 = Unit({
        unitType: 'tank',
        ownerId:  'player2',
        x:        80,
        y:        50,
    });


    gameState.units.add(unit3);
    gameState.units.add(unit4);
    gameState.units.add(unit5);


    // var unit5 = Unit({
    //     unitType: 'tank',
    //     ownerId:  'player2',
    //     x:        82,
    //     y:        50,
    // });
    // gameState.units.add(unit5);



    return gameState;
};

// gets game state from server
var bootState = {
    app:  app,
    init: function() {
        app.gameState = initGameState();
    },
    create: function() {
        var game = this.game;
        app.mapName = 'test';
        game.state.start('preloader', false);
    }
};


module.exports = bootState;
