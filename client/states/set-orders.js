'use strict';

var app         = require('../app');

var setOrdersState = {
    app: app,

    selectedObj: null,

    clearSelected: function() {
        if (this.selectedObj) {
            this.selectedObj.selected(false);
        }
        this.selectedObj = null;
    },
    setSelected: function(obj) {
        if (this.selectedObj) {
            if (obj === this.selectedObj) {
                return;
            }
            this.selectedObj.selected(false);
        }
        obj.selected(true);
        this.selectedObj = obj;

        var unit = this.getSelectedUnit();
        if(unit){
            app.setSelectedUnitText(unit);
        }
    },

    getSelectedUnit: function() {
        if (this.selectedObj) {
            if (this.selectedObj.spriteType === 'unit') {
                return this.selectedObj;

            } else if (this.selectedObj.spriteType === 'order') {
                return this.selectedObj.unitSprite;
            }
        }
    },

    onClickBg: function() {
        var game = this.game;
        var x    = game.input.mousePointer.x;
        var y    = game.input.mousePointer.y;

        if (this.selectedObj) {
            var unit = this.getSelectedUnit();
            if (unit.ownerId !== app.playerId) {
                return;
            }
            var unitSprite = this.getSelectedUnit();
            var order      = this.addOrder({
                unitSprite: unitSprite,
                x:          x,
                y:          y
            });

            unitSprite.addOrder(order);
            this.setSelected(order);
        }
    },

    addOrder: function(settings) {
        var order = app.addOrder(settings);
        this.bindOrder(order);
        return order;
    },
    bindOrder: function(order) {

        order.input.enableDrag();
        order.line.inputEnabled            = true;
        order.line.input.pixelPerfectClick = true;
        order.line.input.pixelPerfectOver  = true;
        order.line.input.useHandCursor     = true;

        order.events.onInputUp.add(function(sprite, point) {
            this.setSelected(sprite);
        }, this);

        order.events.onDestroy.add(function() {
            order.line.destroy();

            if (this.selectedObj === order) {
                this.selectedObj = null;
                var unitSprite   = order.unitSprite;
                var nextSelected = unitSprite.getSelectedOrderFallback(order);
                this.setSelected(nextSelected);
            }
        }, this);

        order.line.events.onInputDown.add(function() {

            var x          = this.game.input.mousePointer.x;
            var y          = this.game.input.mousePointer.y;
            var unitSprite = order.unitSprite;
            var index      = unitSprite.orders.indexOf(order);
            var newOrder   = this.addOrder({
                x:          x,
                y:          y,
                unitSprite: unitSprite
            });
            unitSprite.addOrder(newOrder, index);
            this.setSelected(newOrder);
            this.draggingCreated = newOrder;

        }, this);

        return order;
    },

    bindUnit: function(unitSprite) {
        var state = this;
        unitSprite.events.onInputUp.add(function(sprite, point) {
            this.setSelected(sprite);
        }, this);

        _.each(unitSprite.orders, function(order) {
            state.bindOrder(order);
        });

        return unitSprite;
    },

    getTurnOrdersJson: function() {
        var turnOrders = [];

        _.each(app.units, function(unit) {
            var orders = unit.ordersToJSON();

            orders.forEach(function(order) {
                order.unitId = unit.unitId;
                order.dest   = app.pixelToTileCoords(order.dest.x, order.dest.y);
            });

            turnOrders.push({
                unitId: unit.unitId,
                orders: orders
            });
        });

        return turnOrders;
    },


    init:    function() {},
    preload: function() {},
    create:  function() {
        var state = this;

        _.each(app.units, function(unit) {
            state.bindUnit(unit);
        });

        // game.plugins.add(Phaser.Plugin.Inspector);

        app.mapSprite.events.onInputUp.add(this.onClickBg, this);
        var game = this.game;

        // app.gameState.units.each(function(unit){
        //        console.log('');
        //     console.log('u', unit.id);

        //      _.each(unit.weapons, function(weapon) {
        //         console.log('w', weapon.unit.id);
        //      });
        // });

        // var playerId = 'player1';
        // var enemies   = app.gameState.units.enemiesOfPlayer(playerId);

        // console.log('player', playerId);
        // console.log('enemies', _.map(enemies, 'id'));

        //         var playerId = 'player2';
        // var enemies   = app.gameState.units.enemiesOfPlayer(playerId);

        // console.log('player', playerId);
        // console.log('enemies', _.map(enemies, 'id'));


        // app.gameState.units.each(function(unit){
        //     console.log('unit', unit.id, unit.ownerId);
        // })


        // bmpText.inputEnabled = true;

        // bmpText.input.enableDrag();

        var unitSprite = app.units[1];

        var order = this.addOrder({
            unitSprite: unitSprite,
            x:          374,
            y:          369

        });

        unitSprite.addOrder(order);
        this.setSelected(order);

        var order = this.addOrder({
            unitSprite: unitSprite,
            x:          273,
            y:          307

        });

        unitSprite.addOrder(order);
        this.setSelected(order);



         var unitSprite = app.units[0];

        var order = this.addOrder({
            unitSprite: unitSprite,
            x:          74,
            y:          300

        });

        unitSprite.addOrder(order);


        // var order = this.addOrder({
        //     unitSprite: unitSprite,
        //     x:          273,
        //     y:          307

        // });

        // unitSprite.addOrder(order);


    },
    update: function() {
        var game = this.game;

        app.orderRenderer.update(app.units);

        if (this.draggingCreated) {
            this.draggingCreated.x = game.input.mousePointer.x;
            this.draggingCreated.y = game.input.mousePointer.y;
            if (game.input.activePointer.leftButton.isUp) {

                this.draggingCreated = null;
            }
        }

        if (app.keys.delete.justUp) {
            if (this.selectedObj) {
                if (this.selectedObj.spriteType === 'order') {
                    this.selectedObj.destroy();
                }
            }
        }

        if (app.keys.clear_selected.justUp) {
            this.clearSelected();
        }

        if (app.keys.end_turn.justUp) {
            console.log('end turn');
            var turnOrdersJson = this.getTurnOrdersJson();
            app.sendTurnOrders(turnOrdersJson);
        }
    },

    render: function() {


        var game = this.game;
        app.units.forEach(function(unit) {
            // game.debug.text('- ' + unit.currentOrderId, unit.x, unit.y);

            game.debug.geom(unit.circle, '#cfffff', false);


            // game.debug.spriteInfo(unit, 40, 40);
            //     ent.orders.forEach(function(order) {
            // game.debug.spriteBounds(unit);

            //         // game.debug.spriteBounds(order.line);
            //         // game.debug.spriteInfo(order.line, 50, 50);
            //     });

        });

        // game.debug.text('Click the blue squares then the map to set move order waypoints', 50, 520);
        // game.debug.text('Click+drag wayponts or waypoint lines to add waypoints between', 50, 540);
        // game.debug.text('Keys: Z: delete selected order, E: execute orders', 50, 560);
        var selected = this.getSelectedUnit();

        if(selected){
            var unit = app.gameState.units.get(selected.unitId);

                game.debug.text('Phaser & Pixi\nrocking!', 20, 20);

            game.debug.text(selected.unitState.name, 10, 520)
            game.debug.text('speed: ' + selected.unitState.speed, 10, 540)
            game.debug.text('hp: ' + selected.hp + '/' + selected.hpMax, 10, 560)

        }

    // game.debug.geom(this.dragBounds, '#0fffff');
    },
    shutdown: function() {

        var state = this;
        _.each(app.units, function(unit) {
            unit.events.removeAll(state);
        });

        app.mapSprite.events.removeAll(this);

        this.units       = [];
        this.selectedObj = null;
    }
};

module.exports = setOrdersState;
