'use strict';
var app = require('../app');

var TurnAnimator = require('../lib/turn-animator');

var animateTurnState = {
    app: app,

    turnResults: null,

    init: function(unitTurnResults) {
        console.log('unitTurnResults', unitTurnResults);
        this.turnResults = unitTurnResults;
    },
    create: function() {
        _.each(app.units, function(unit) {

            // unit.rangeCircle.followCoords(unit);
            unit.rangeCircle.visible = false;
            _.each(unit.orders, function(order) {
                order.input.disableDrag();
                order.line.inputEnabled = false;
            });
        });

        var tickAnimationTime = 400;
        var turnAnimator      = TurnAnimator({
            game:              this.game,
            turnResults:             this.turnResults,
            tickAnimationTime: tickAnimationTime,
        });

        turnAnimator.validate();
        turnAnimator.queue();
        turnAnimator.play();

        this.turnAnimator = turnAnimator;
    },

    update: function() {
        app.orderRenderer.update(app.units);
    },
    render: function() {


        var game = this.game;


        if (this.turnAnimator.timer) {
            var timer   = this.turnAnimator.timer;
            var elapsed = timer.seconds.toFixed(1);
            this.lastElapsed = elapsed;
            this.game.debug.text('Elapsed: ' + elapsed, 50, 540);
        } else{
            this.game.debug.text('Elapsed: ' + this.lastElapsed, 50, 540);
        }

        game.debug.text('Tick: ' + (app.units[2].tickId + 1).toFixed(0), 50, 520);

        // var unit = app.getUnitById(2);
        // game.debug.text('- ' + unit['weapons.primary.cooldownRemaining'], 200, 300);

        app.units.forEach(function(unit) {





            // game.debug.geom(unit.circle, '#cfffff', false);


            // game.debug.spriteInfo(unit, 40, 40);
            //     ent.orders.forEach(function(order) {
            // game.debug.spriteBounds(unit);

            //         // game.debug.spriteBounds(order.line);
            //         // game.debug.spriteInfo(order.line, 50, 50);
            //     });

        });



    // game.debug.geom(this.dragBounds, '#0fffff');
    },
};


module.exports = animateTurnState;
