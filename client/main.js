'use strict';

var _ = require('lodash');

require('./lib/phaser-extend');

global._        = _;
global.tileSize = 5;

var app = require('./app');

var states = {
    boot:           require('./states/0_boot'),
    preloader:      require('./states/1_preloader'),
    init:           require('./states/2_init'),
    deploy:         require('./states/deploy'),
    'set-orders':   require('./states/set-orders'),
    'animate-turn': require('./states/animate-turn'),
};

var settings = {
    width:    800,
    height:   600,
    renderer: Phaser.WEBGL,
    // parent: 'game',
    transparent: false,
    antialias:   false,
    // state: null,
    // scaleMode: Phaser.ScaleManager.EXACT_FIT,
    enableDebug: true,
// resolution: null,
// preserveDrawingBuffer: null,
// physicsConfig: null,
// seed: null,
};

var game = new Phaser.Game(settings);

_.each(states, function(state, key) {
    game.state.add(key, state);
});

app.init(game);

game.state.start('boot');
window.game = game;
window.app  = app;
