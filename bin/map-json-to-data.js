'use strict';
var fs        = require('fs');
var bb        = require('bluebird');
var path      = require('path');
var writeFile = bb.promisify(fs.writeFile);

var img = process.argv[2];

var base = path.basename(img, '.png');

var colorListJson = './dest/' + base + '-color-list.json';
var mapDataJson   = './dest/' + base + '-data.json';

var colorList = require(colorListJson);
var mapData   = require(mapDataJson);

var width  = mapData[0].length;
var height = mapData.length;

for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
        var val = mapData[y][x];
        var meta = colorList[val].meta;
        mapData[y][x] = meta;
    }
}

var dest = './dest/' + base + '-map.json';
writeFile(dest, JSON.stringify(mapData));
