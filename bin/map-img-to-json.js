'use strict';
var fs   = require('fs');
var bb   = require('bluebird');
var path = require('path');

var stringify = require('json-stringify');
var writeFile = bb.promisify(fs.writeFile);

var getColors = require('./lib/get-colors');

var file = process.argv[2];


var base = path.basename(file, '.png');

getColors({
    src: file,
})
    .then(function(result) {

        var colorList =  result.colorList;
        var colorListFile = './dest/' + base + '-color-list.json';
        var colorListContents = stringify(colorList, null, 4);

        var data = result.data;
        var dataFile = './dest/' + base + '-data.json';
        var dataContents = JSON.stringify(data);

        return bb.all([
            writeFile(colorListFile, colorListContents),
            writeFile(dataFile, dataContents),
        ]);

    })
    .then(function(result) {

    })
    .catch(function(err) {
        console.error(err);
    });
