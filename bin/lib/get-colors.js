'use strict';

var Jimp = require('jimp');

var initData = function(width) {
    var data = [];

    for (var i = 0; i < width; i++) {
        if (data[i] === void 0) {
            data[i] = [];
        }
    }
    return data;
}

var getColors = function(settings) {
    settings = settings || {};

    var src  = settings.src;
    var rgba = settings.rgba;

    // open a file called "lenna.png"
    return Jimp.read(src)
        .then(function(image) {

            var width  = image.bitmap.width;
            var colors = [];
            var data   = initData(width);

            image.scan(0, 0, image.bitmap.width, image.bitmap.height, function(x, y, idx) {
                // x, y is the position of this pixel on the image
                // idx is the position start position of this rgba tuple in the bitmap Buffer
                // this is the image

                var red   = this.bitmap.data[idx + 0];
                var green = this.bitmap.data[idx + 1];
                var blue  = this.bitmap.data[idx + 2];
                var alpha = this.bitmap.data[idx + 3];

                var color;
                if (rgba) {
                    color = 'rgba(' + [red, green, blue, alpha].join(',') + ')';
                } else {
                    color = 'rgb(' + [red, green, blue].join(',') + ')';
                }



                var index = colors.indexOf(color);
                if(index === -1){
                    colors.push(color);
                    index = colors.length - 1;
                }

                data[y][x] = index;
            });


            var colorList = {};

            for (var i = 0; i < colors.length; i++) {
                var color = colors[i];
                colorList[i] = {
                    name:  'color_' + i,
                    color: color,
                    meta:  {},
                };
            }

            return {
                colorList: colorList,
                data:      data,
            };
        });
};
module.exports = getColors;

