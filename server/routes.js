'use strict';

var express      = require('express');
var router       = express.Router();

require('./controllers/auth')(router);
require('./controllers/game')(router);
require('./controllers/map')(router);


router.get('/test',
    function(req, res) {
        // console.log('route req.session', req.session);
        // console.log('route req.cookies', req.cookies);
        // console.log('route req.user', req.user);
        res.render('test');
    });



module.exports = router;
