'use strict';

require('dotenv').config({silent: true});

var server = require('./services/server');
var c      = require('./config');

var port    = c.server.port;
var hostUrl = c.server.url;

server.listen(port, function() {
    console.log('Open ' + hostUrl);
});

// var PrimusMessage = require('../shared/primus-message.js');
// var EntityCollection = require('../shared/entity-collection.js');
// var Game             = require('./lib/game.js');

// // instances
// var gameStore = require('./services/db').games;
// var primus    = require('./services/primus');



// var game = Game();
// var msg = PrimusMessage();

// game.gameState.entities.create({
//     entityType: 'tank',
//     name:       'alpha',
//     owner:      'player_1',
// });

// game.gameState.entities.create({
//     entityType: 'tank',
//     name:       'beta',
//     owner:      'player_2',

//     color:      'blue',
//     x:          0,
//     y:          5,
// });

// primus.on('connection', function(spark) {

//     spark.on('data', function(data) {
//         msg.onData(data, spark);
//     });
// });

// msg.onReceive('game_connect', function(data, spark) {

//     var gameId = data.gameId;

//     gameStore.getActive(gameId, function(err, game) {});

// // spark.write({
// //     type:      'init_game',
// //     gameState: game.gameState.toJSON()
// // })
// });

// msg.onReceive('init_game', function(data, spark) {
//     spark.write({
//         type:      'init_game',
//         gameState: game.gameState.toJSON()
//     })
// });

// msg.onReceive('turn_orders', function(data, spark) {
//     console.log(spark.id, 'turn_orders:', data);

//     var result = game.getTurnResults(data.turnOrders);
//     result.type = 'turn_result';

//     spark.write(result);
// });
