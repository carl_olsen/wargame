'use strict';

var _ = require('lodash');
// throw Error('foo');
var GameState        = require('../../shared/game-state.js');
var EntityTurn       = require('../../shared/unit-tick-resolver.js');

var c = require('../../shared/config.js');
var db     = require('../services/db').games;
var rules = require('../../shared/rules');
var ticks_per_turn = c.ticks_per_turn;


var Player = function(settings) {
    settings = settings || {};

    var out = {
        number:   null,
        _id:       null,
        email:    null,
        sparkId:  null,
        spark:    null,
        name:     null,
        orders:   null,
        setSpark: function(spark) {
            this.sparkId = spark.id;
            this.spark   = spark;
            spark.on('end', this.reset, this);
        },
        reset: function() {
            this.spark   = null;
            this.sparkId = null;
            this.orders  = null;
        },
        clearOrders: function() {
            this.orders = null;
        },
        toJSON: function(){
            return _.pick(this, [
                '_id',
                'number',
                'sparkId',
                'name',
                'email'
            ]);
        },
    }
    return Object.assign(out, settings);
};

Player.fromJSON = function(json){
    return Player(json);
};

var Game = function(settings, stateSettings) {
    settings = settings || {};

    var out = {
        ticksPerTurn: ticks_per_turn,

        _id:        null,
        name:      null,
        gameState: GameState(stateSettings),
        player1:   Player({number: 1}),
        player2:   Player({number: 2}),
        turnLog: [],
        turnNumber: 1,

        setPlayer1: function(user){
            this.player1 = Player.fromJSON(user);
        },

        setPlayer2: function(user){
            this.player2 = Player.fromJSON(user);
        },

        playerBySparkId: function(sparkId) {
            if (this.player1.sparkId === sparkId) {
                return this.player1;
            }

            if (this.player2.sparkId === sparkId) {
                return this.player2;
            }
        },

        getStatus: function() {
            var out = [];

            if (!this.player2.sparkId) {
                out.push('waiting for player 2 join');
            }

            if (!this.player1.orders) {
                out.push('Waiting for Player 1 turn orders');
            }

            if (!this.player2.orders) {
                out.push('Waiting for Player 2 turn orders');
            }
            return out;
        },

        setTurnOrders: function(sparkId, turnOrdersJson) {
            var player = this.playerBySparkId(sparkId);

            if(player.orders){
                console.error('tring to send orders a second time', sparkId, turnOrdersJson);
                return;
            }
            // @TODO validate that player owns all entities in orders
            player.orders = turnOrdersJson;

            if (this.player1.orders && this.player2.orders) {
                this.processTurn();
            }
        },

        processTurn: function() {

            var player1Orders = this.player1.orders.map(function(eTurnJson) {
                return EntityTurn.fromJSON(eTurnJson);
            });

            var player2Orders = this.player2.orders.map(function(eTurnJson) {
                return EntityTurn.fromJSON(eTurnJson);
            });

            var entityTurnResults = rules.getTurnResults(this.gameState, player1Orders, player2Orders, this.ticksPerTurn);

            // @TODO filter data returned to users by visibility
            var p1EntityTurnResults = entityTurnResults;
            var p2EntityTurnResults = entityTurnResults;

            var gameState = this.gameState.toJSON();

            // @TODO filter data returned to users by visibility
            var p1GameState = gameState;
            var p2GameState = gameState;

            var turnLog = {
                results: entityTurnResults,
                gameState: gameState,
                turn: this.turnNumber,
                player1: {
                    orders: this.player1.orders,
                    results: p1EntityTurnResults,
                    gameState: p1GameState,
                },
                player2: {
                    orders: this.player2.orders,
                    results: p2EntityTurnResults,
                    gameState: p2GameState,
                },
            };

            db.saveTurn(this._id, turnLog);

            this.player1.spark.write({
                type: 'turn_results',
                turn: this.turnNumber,
                entityTurnResults: p1EntityTurnResults,
                finalState:        p1GameState,
            });

            this.player2.spark.write({
                type: 'turn_results',
                turn: this.turnNumber,
                entityTurnResults: p2EntityTurnResults,
                finalState:        p2GameState,
            });

            this.player1.clearOrders();
            this.player2.clearOrders();

            this.turnNumber++;
        },


        toJSON: function(){
            var out = _.pick(this, [
                'id',
                'name',
                'turnLog',
                'turnNumber',
            ]);
            out.player1 = this.player1.toJSON();
            out.player2 = this.player2.toJSON();
            out.gameState = this.gameState.toJSON();
            return out;
        }
    };

    return Object.assign(out, settings);
};

Game.fromJSON = function(json){
    if(json.player1){
        json.player1 = Player.fromJSON(json.player1);
        json.player1.number = 1;
    }

    if(json.player2){
        json.player2 = Player.fromJSON(json.player2);
        json.player2.number = 2;
    }

    json.gameState = GameState.fromJSON(json.gameState);

    return Game(json);
};


module.exports = Game;