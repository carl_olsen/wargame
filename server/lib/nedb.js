'use strict';

// var PromisifyMe = require('promisify-me');

// var nedb = PromisifyMe(require('nedb'), 'nedb');

// nedb.prototype.findC = nedb.prototype.find;
// nedb.prototype.find = function(query, projection){
//     var cursor = this.findC(query, projection);
//     return cursor.exec();
// };

// nedb.prototype.findOneC = nedb.prototype.findOne;
// nedb.prototype.findOne = function(query, projection){
//     var cursor = this.findOneC(query, projection);
//     return cursor.exec();
// };

// module.exports = nedb;


var NedbDatastore = require('nedb')
var thenify = require('thenify')

function make(DB){
    var newDB = { nedb: DB }

    var methods = [ 'loadDatabase', 'insert', 'find', 'findOne', 'count', 'update', 'remove', 'ensureIndex', 'removeIndex' ]
    for (var i = 0; i < methods.length; ++i) {
        var m = methods[i]
        newDB[m] = thenify(DB[m].bind(DB))
    }

    newDB.cfind = function(query) {
        var c = DB.find(query)
        c.exec = thenify(c.exec.bind(c))
        return c
    }

    newDB.cfindOne = function(query) {
        var c = DB.findOne(query)
        c.exec = thenify(c.exec.bind(c))
        return c
    }

    newDB.ccount = function(query) {
        var c = DB.count(query)
        c.exec = thenify(c.exec.bind(c))
        return c
    }

    return newDB
}

function datastore(options) {
    var DB = new NedbDatastore(options)
    return make(DB);
}
// so that import { datastore } still works:
datastore.datastore = datastore
datastore.fromInstance = make;

module.exports = datastore