'use strict';
var Game = require('../game');
var bb   = require('bluebird');

var GameStore = function GameStore(promiseDataStore, settings) {
    settings = settings || {};
    var userDb      = settings.userDb;
    var db          = promiseDataStore;
    var activeGames = {};

    var methods = {
        all: function() {
            return db.find({});
        },
        get: function(id) {
            return db.findOne({
                _id: id
            });
        },
        create: function(data) {
            return db.insert(data);
        },
        update: function(id, data) {
            return db.update({
                _id: id
            }, data);
        },
        delete: function(id) {
            return db.remove({
                _id: id
            });
        },
        load: function(gameId) {
            return this
                .get(gameId)
                .then(function(result) {
                    var p1;
                    var p2;
                    if (result.player1) {
                        p1 = userDb.get(result.player1);
                    }

                    if (result.player2) {
                        p2 = userDb.get(result.player2);
                    }
                    return bb.props({
                        gameJson: result,
                        player1: p1,
                        player2: p2
                    });

                })
                .then(function(result) {

                    var gameJson = result.gameJson;
                    var player1 = result.player1;
                    var player2 = result.player2;

                    if (player1) {
                        gameJson.player1 = Object.assign(gameJson.player1, player1);
                    }
                    if (player2) {
                        gameJson.player2 = Object.assign(gameJson.player2, player2);
                    }

                    return Game.fromJSON(gameJson);
                })
                .catch(function(err) {
                    console.error(err);
                })
        },

        getActive: function(id) {
            if (!activeGames[id]) {
                return this
                    .load(id)
                    .then(function(game) {
                        activeGames[id] = game;
                        return game;
                    });
            }

            return Promise.resolve(activeGames[id]);
        },

        saveTurn: function(id, turnLog, cb) {
            var query = {
                _id: id
            };
            var data = {
                $push: {
                    turnLog: turnLog
                }
            };
            return db.update(query, data);
        },
    };

    return Object.create(methods);
};
module.exports = GameStore;
