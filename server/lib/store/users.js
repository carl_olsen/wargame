'use strict';

var UserStore = function UserStore(promiseDataStore, settings){
    settings = settings || {};
    var db = promiseDataStore;

    var methods = {
        all: function(){
            return db.find({});
        },
        get: function(id){
            return db.findOne({ _id: id });
        },
        getByEmail: function(email){
            return db.findOne({ email: email });
        },
        create: function(data) {
            return db.insert(data);
        },
        update: function(id, data) {
            return db.update({ _id: id }, data);
        },
        delete: function(id) {
            db.remove({ _id: id });
        },
    };

    var out = Object.create(methods);
    return Object.assign(out, settings);
};
module.exports = UserStore;
