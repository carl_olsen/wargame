'use strict';

var path = require('path');

var basePath = __dirname + '/../../shared/maps/';

var forceExt = function(file, ext) {
    return file.substr(0, file.lastIndexOf('.')) + '.' + ext;
};

var getFile = function(name, ext) {

    var file = basePath + forceExt(name, ext);
    return path.normalize(file);
};

module.exports = function(router) {
    var r = router;

    r.get('/map/json/:name', function(req, res) {
        var name = req.params.name;
        var file = getFile(name, 'json');
        res.sendFile(file);
    });

    r.get('/map/img/:name', function(req, res) {

        var name = req.params.name;
        var file = getFile(name, 'png');
        res.sendFile(file);
    });
};