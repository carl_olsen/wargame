'use strict';

var passwordless = require('../services/middleware/passwordless');
var gameStore    = require('../services/db').games;

module.exports = function(router) {
    var r = router;

    r.get('/game/list',
        passwordless.restricted(),
        function(req, res) {
            gameStore
                .all()
                .then(function(results) {
                    // console.log(arguments);
                    res.render('game-list', {
                        games: results
                    });
                });
        });

    r.get('/game/create',
        passwordless.restricted(),
        function(req, res) {
            res.render('game-create');
        });

    r.post('/game/create',
        passwordless.restricted(),
        function(req, res) {

            var user    = res.locals.user
            var name    = req.body.name;
            var newGame = {
                name:    name,
                player1: {
                    _id:    user._id,
                    number: 1,
                    name:   user.name,
                    email:  user.email,
                }
            };

            gameStore
                .create(newGame)
                .then(function(game) {
                    var data = {
                        link: '/game/' + game._id
                    };
                    res.render('game-created', data);
                });
        });

    r.get('/game/:game_id',
        passwordless.restricted(),
        function(req, res) {
            var user   = req.userModel;
            var gameId = req.params.game_id;

            var done = function(game){
                res.render('game', {
                    game:    game,
                    player1: game.player1,
                    player2: game.player2
                });
            };
            gameStore
                .getActive(gameId)
                .then(function(game){

                    if (!game) {
                        res.status(404).send('Game not found');
                        return;
                    }

                    var isPlayer1  = user._id === game.player1._id;
                    var hasPlayer2 = game.player2._id;

                    if (!isPlayer1 && !hasPlayer2) {
                        game.setPlayer2(user);

                        var newData = {
                            player2: {
                                _id:   user._id,
                            }
                        };
                        gameStore.update(game._id, newData, function(err, result) {
                            done(game);
                        });
                        return;
                    }

                    var isPlayer2 = user._id === game.player2._id;

                    if (isPlayer1 || isPlayer2) {
                        done(game);
                    } else {
                        res.status(401).send('Not your game');
                    }


                }).catch(function(err){
                    console.error(err);
                });

    });
};