'use strict';

var passwordless = require('passwordless');
var userStore    = require('../services/db').users;

module.exports = function(router){
    var r = router;

    r.get('/login',
        function(req, res) {
            res.render('auth/login');
        });

    r.get('/logout',
        passwordless.logout(),
        function(req, res) {
            res.redirect('/');
        });

    r.post('/sendtoken',
        passwordless.requestToken(
            function(email, delivery, cb, req) {
                userStore
                    .getByEmail(email)
                    .then(function(user) {
                        if (user) {
                            cb(null, user._id);

                        } else {
                            userStore.create({
                                email: email
                            })
                            .then(function(result) {
                                console.log('create');
                                cb(null, result._id);
                            })
                            .catch(function(err){
                                console.error(err);
                                cb(err);
                            });
                        }
                    });
            }),
        function(req, res) {
            res.render('auth/register-sent');
        });


};