'use strict';

module.exports = function(primus) {

    primus.on('connection', function(spark) {

        // spark.send('hi', 'good morning');

        // emit to news with ack
        // var data ={
        // msg: 'good morning'
        // };
        //
        var data = {};

        spark.send('news', data, function(data) {
            console.log(data); // => 'by client'
        });

        // respond to request callback
        spark.on('get_foo', function(data, send) {
            console.log('get_foo', data);

            // response
            send({
                id: data.id,
                foo:'bar'
            });

        });

        // // receive incoming sport messages
        // spark.on('sport', function(data) {
        //     console.log('sport', data); // => ping-pong
        // });

    });

};