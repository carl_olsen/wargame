'use strict';

var Primus     = require('primus');
var primusEmitter = require('primus-emitter');

var server        = require('../server');
var passwordless  = require('../middleware/passwordless');
var cookies       = require('../middleware/cookies');
var sessionStore  = require('../middleware/session/store');
var userInjector  = require('../middleware/user');
var primusSession = require('../../lib/primus-session')({
    store: sessionStore
});

var primus = new Primus(server, {
    transformer: 'websockets',
    rooms:       {
        wildcard: false
    }
});

primus.use('emit', primusEmitter);

primus.before('cookies', cookies);
primus.before('session', primusSession);
primus.before('passwordless-session', passwordless.sessionSupport());
primus.before('user-injector', userInjector);

// move to end
primus.remove('authorization');
primus.before('authorization', require('primus/middleware/authorization'));

primus.authorize(function(req, done) {
    if (!req.userModel) {
        return done('Not Authorized');
    }
    done();
});

require('./events')(primus);


module.exports = primus;