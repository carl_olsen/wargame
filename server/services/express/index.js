'use strict';

var express    = require('express');
var bodyParser = require('body-parser');

var cookies        = require('../middleware/cookies');
var passwordless   = require('../middleware/passwordless');
var userInjector   = require('../middleware/user');
var expressSession = require('../middleware/session')
var view           = require('./view');
var routes         = require('../../routes.js');
var c              = require('../../config');

var viewsPath = c.server.views_dir;
var ext       = view.extname;

var staticFiles = express.static(c.server.public_dir);

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cookies);
app.use(expressSession);

app.use(passwordless.sessionSupport());
app.use(passwordless.acceptToken());
app.use(userInjector);

app.engine(ext, view.engine);
app.set('view engine', ext);
app.set('views', viewsPath);

app.use('/', routes);
app.use(staticFiles);

module.exports = app;