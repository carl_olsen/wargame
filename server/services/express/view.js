'use strict';

var exphbs = require('express-handlebars');
var c      = require('../../config');

var viewsPath = c.server.views_dir;

var hbs = exphbs.create({
    extname:       '.hbs',
    partialsDir:   viewsPath + '/partials',
    layoutsDir:    viewsPath + '/layouts',
    defaultLayout: 'base',
    helpers:       {}
});

module.exports = hbs;


