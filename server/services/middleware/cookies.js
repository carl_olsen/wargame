'use strict';

var cookieParser = require('cookie-parser');
var c            = require('../../config');

module.exports = cookieParser(c.secret);