'use strict';

var session          = require('express-session');
var NedbStoreSession = require('express-nedb-session')(session);

var filename = 'storage/session.db';

var store = new NedbStoreSession({
    filename: filename
});

module.exports = store;