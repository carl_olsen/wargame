'use strict';

var session          = require('express-session');
var store = require('./store');

var secret   = 'secret123';

var expressSession = session({
    secret:            secret,
    resave:            false,
    saveUninitialized: false,
    cookie:            {
        path:     '/',
        httpOnly: true,
        maxAge:   365 * 24 * 3600 * 1000 // One year for example
    },
    store: store
});


module.exports = expressSession;