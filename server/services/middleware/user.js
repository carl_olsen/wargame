'use strict';

var userStore           = require('../db').users;

// passwordless user model injector
module.exports = function injectUserMiddleware(req, res, next) {
    if(req.user) {
        userStore
            .get(req.user)
            .then(function(user) {
                req.userModel = user;
                if(res.locals){
                    res.locals.user = user;
                }

                next();
            })
            .catch(function(err){
                console.error(err);
                next();
            });

    } else {
        next();
    }
};