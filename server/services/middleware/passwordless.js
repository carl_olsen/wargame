
'use strict';

var passwordless   = require('passwordless');
var NedbStoreToken = require('passwordless-nedbstore');
var email          = require('emailjs');

var c            = require('../../config.js');

// var smtpServer = email.server.connect({
//     user:     c.smtp.user,
//     password: c.smtp.pass,
//     host:     c.smtp.host,
//     ssl:      false,
// });

passwordless.init(new NedbStoreToken({
    filename: c.storage_files.token,
    autoload: true,
}));

// Setup of Passwordless
passwordless.addDelivery(
    function(tokenToSend, uidToSend, recipient, callback) {


        var host      = c.server.url;
        var subject   = 'Token for ' + host;
        var fromEmail = c.mail.from_address;
        var url = host + '?token=' + tokenToSend + '&uid=' + encodeURIComponent(uidToSend);
        console.log('url', url);
        callback(null);
        // var content   = 'Hello!\nYou can now access your account here: ' + url;

        // // Send out token
        // smtpServer.send({
        //     text:    content,
        //     from:    fromEmail,
        //     to:      recipient,
        //     subject: subject
        // }, function(err, message) {
        //     if (err) {
        //         console.error(err, message);
        //         throw err;
        //     }
        //     callback(err);
        // });
    });

module.exports = passwordless;