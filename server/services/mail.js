'use strict';

var email = require('emailjs');
var c     = require('../../config.js');

var smtpServer = email.server.connect({
    user:     c.smtp.user,
    password: c.smtp.pass,
    host:     c.smtp.host,
    ssl:      false,
});

module.exports = smtpServer;