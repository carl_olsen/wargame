'use strict';

var DataStore = require('../lib/nedb');
var UserStore = require('../lib/store/users');
var GameStore = require('../lib/store/games');
var c         = require('../config');

var userDS = DataStore({
    filename: c.storage_files.users,
    autoload: true,
});

var users = UserStore(userDS);

var gameDS = DataStore({
    filename: c.storage_files.games,
    autoload: true,
});

var games = GameStore(gameDS,  {
    userDb: users
});


module.exports = {
    games: games,
    users: users
};