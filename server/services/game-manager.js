'use strict';

/*globals Promise:true*/

var Game = require('./index').Game;

var activeGames = {};

var gameManager = {
    getActive: function(id) {
        if (!activeGames[id]) {

            Game
            .find(id, {with: ['player1', 'player2']})
            .then(function(gameJson){
                var game = Game.fromJSON(gameJson);
                activeGames[id] = game;
                return game;
            })
            .catch(function(err){
                console.error(err);
            });
        }

        var p = new Promise(function(resolve, reject) {
            resolve(activeGames[id]);
        });

        var p = Promise.resolve(activeGames[id])

        return p;

    }
};


module.exports = gameManager;