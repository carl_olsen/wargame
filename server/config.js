'use strict';

var deepFreeze = require('deep-freeze-strict');

var e = process.env;

var config = {
    secret: e.SECRET,

    server: {
        protocol: 'http',
        domain: 'localhost',
        port: e.SERVER_PORT || 8080,
        public_dir: 'public',
        views_dir: 'views',
    },

    mail: {
        from_address: 'test@unstoppableCarl.com',
    },

    smtp: {
        user: e.SMTP_USER,
        pass: e.SMTP_PASS,
        host: e.SMTP_HOST,
    },

    storage_files: {
        token:    'storage/token.nedb',
        users:    'storage/users.nedb',
        games:    'storage/games.nedb',
        sessions: 'storage/sessions.nedb',
    },

};


var s = config.server;

config.server.url = s.protocol + '://' + s.domain + ':' + s.port;

module.exports = deepFreeze(config);

