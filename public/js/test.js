/* global Phaser */
/* global window */
/* global _ */

'use strict';

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'game');


var applyKeyframe = function(target, keyframeData){

    for(var key in keyframeData){
        target[key] = keyframeData[key];
    }
};

var gameState = {

    init:    function() {},
    preload: function() {},
    create:  function() {

        var obj = {
            a: 0
        };


var keyframes = [
{a: 1},
{a: 2},
{a: 3},
];

        var tween = this.game.add.tween(obj)
        .to({a: 1}, 200)
        .to({a: 2}, 200)
        .to({a: 3}, 200);

        var data = tween.generateData();

        console.log('data', data);

    },
    update: function() {},
    render: function() {},
};






window.onload = function() {
    game.state.add('game', gameState, true);
};