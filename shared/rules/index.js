'use strict';

var _                   = require('lodash');
var terrain             = require('./terrain');
var util                = require('../util.js');
var getDistance         = util.getDistance;
var getNormalizedVector = util.getNormalizedVector;
var c                   = require('../config');

var rules = {
    getSpeedMod: function(unit, map, x, y) {

        var tileMeta = map.getTile(x, y);
        var type     = tileMeta.type;
        var typeMeta = terrain[type];
        var speedMod = typeMeta.speedMod;
        return speedMod;
    },

    calculateUnitMove: function(settings) {
        var defaults = {
            map:  null,
            unit: null,
            dest: null,

            speed:    null,
            startPos: null,
        };

        var s    = _.defaults(settings, defaults);
        var map  = s.map;
        var unit = s.unit;
        var dest = s.dest;

        var unitSpeed = (s.speed || unit.speed) / c.ticks_per_turn;
        var pos       = s.startPos || unit.pos();
        var dist      = getDistance(pos, dest);
        var posMod    = rules.getSpeedMod(unit, map, pos.x, pos.y);
        var speed               = unitSpeed * posMod;
        var willPassDestination = dist < speed;

        var nextPos;
        var remainingSpeed = 0;

        if (willPassDestination) {
            remainingSpeed = (speed - dist) * c.ticks_per_turn;
            nextPos        = {
                x: dest.x,
                y: dest.y,
            };

        } else {
            var nVec = getNormalizedVector(pos, dest, dist);

            var speedVector = {
                x: speed * nVec.x,
                y: speed * nVec.y,
            };

            nextPos = {
                x: pos.x + speedVector.x,
                y: pos.y + speedVector.y,
            };
        }

        return {
            pos:                 nextPos,
            speed:               speed,
            remainingSpeed:      remainingSpeed,
            willPassDestination: willPassDestination,
        };
    },
};

module.exports = rules;