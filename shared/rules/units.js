'use strict';

// var w = require('./weapons');

// max speed 100
var Units = {
    tank: {
        unitType: 'tank',
        name:     'Tank',
        speed:    30,
        hpMax:    10,
        weapons:  {
            primary: {
                weaponType: 'beam'
            }
        }
    },
    scout: {
        unitType: 'scout',
        name:     'Scout',
        speed:    100,
        hpMax:    10,
        weapons:  {
            primary: {
                weaponType: 'mg'
            }
        }
    }
};

module.exports = Units;