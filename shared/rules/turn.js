'use strict';

// prepare results
var initResults = function(tickResolvers, ticksPerTurn) {

    var results = [];
    tickResolvers.forEach(function(resolver) {
        var unitId = resolver.unitId;
        results[unitId] = [];
        for (var i = 0; i < ticksPerTurn; i++) {
            var tickId = i;
            results[unitId][tickId] = {
                unitState:   null,
                tickActions: []
            };
        }
    });
    return results;
};

var getUnitState = function(gameState, unitId) {
    var unit = gameState.units.get(unitId);

    var unitState = {
        hp:                                  unit.hp,
        'weapons.primary.cooldownRemaining': unit.weapons.primary.cooldownRemaining
    };

    return unitState;
};

var getTurnResults = function(gameState, player1UnitTickResolvers, player2UnitTickResolvers, ticksPerTurn) {

    var tickResolvers = [].concat(player1UnitTickResolvers, player2UnitTickResolvers);

    var results = initResults(tickResolvers, ticksPerTurn);

    // resolve turns
    for (var i = 0; i < ticksPerTurn; i++) {
        var tickId = i;

        var pendingActions = [];
        // queue tickActions from tickResolver
        tickResolvers.forEach(function(unitTickResolver) {
            var unitId = unitTickResolver.unitId;

            results[unitId][tickId].unitState = getUnitState(gameState, unitId);

            // get tick actions from unit orders
            var tickActions = unitTickResolver.tick(tickId);
            pendingActions = pendingActions.concat(tickActions);
        });

        // after queuing, apply all tick actions and get result
        pendingActions.forEach(function(tickAction) {
            // returns self and or any other tickActions created as a side effect of resolving this tickAction
            // such as a damage tick action as a side effect of an attack
            var resultingTickActions = tickAction.act() || tickAction;

            // convert single tickActions to array of
            if (!Array.isArray(resultingTickActions)) {
                resultingTickActions = [resultingTickActions];
            }

            resultingTickActions.forEach(function(resultTickAction) {
                var unitId = resultTickAction.unitId;
                results[unitId][tickId].tickActions.push(resultTickAction.toJSON());
            })

        });

    }

    return results;
};

module.exports = getTurnResults;