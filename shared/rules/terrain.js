'use strict';

var terrain = {
    grass: {
        name: 'grass',
        color: 'rgb(0,128,0)',
        speedMod: 1,
    },
    woods: {
        name: 'woods',
        color: 'rgb(0,80,38)',
        speedMod: 0.5,
    },
    water: {
        name: 'water',
        color: 'rgb(0,0,255)',
        speedMod: 0.1,
    },
    sand: {
        name: 'sand',
        color: 'rgb(210,180,140)',
        speedMod: 1.1,
    }
};

module.exports = terrain;