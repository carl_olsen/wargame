'use strict';

var Weapons = {
    mg: {
        weaponType: 'mg',
        name:       'MG',
        range:      40,
        cooldown:   8,
        damage:     2,
    },
    beam: {
        weaponType: 'beam',
        name:       'Beam',
        range:      30,
        cooldown:   10,
        damage:     6,
    }
};

module.exports = Weapons;