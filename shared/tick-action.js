'use strict';

var types = {
    move: require('./tick-action/move.js')
};

var TickAction = function(options) {
    var type    = options.tickActionType;
    var Factory = types[type];
    return Factory(options);
};

module.exports = TickAction;