'use strict';

var types = {
    move: require('./order/move.js')
};

var typeKey = 'orderType';

var Order = function(options) {
    var type    = options[typeKey];
    var Factory = types[type];
    return Factory(options);
};

Order.fromJSON = function(json) {
    var type = json[typeKey];
    var Factory = types[type];
    return Factory.fromJSON(json);
};

module.exports = Order;