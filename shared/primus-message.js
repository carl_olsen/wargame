'use strict';

var PrimusMessage = function(){

    var handlers = Object.create(null);
    var out = {
        onReceive: function(type, func, context){
            if(context){
                func = func.bind(context);
            }
            handlers[type] = func;
        },
        // spark only passed on serverside
        onData: function(data, spark){
            if(!data){
                return;
            }
            var type = data.type;
            if(!type){
                return;
            }
            var handler = handlers[type];
            if(handler){
                handler(data, spark);
            } else {
                console.log('invalid message type', data);
            }
        },
        bindClient: function(primus){
            primus.on('data', this.onData, this);
        },
        bindServer: function(spark){
            spark.on('data', function(data){
                this.onData(data, spark);
            }, this);
        }

    }

    return out;
};

module.exports = PrimusMessage;