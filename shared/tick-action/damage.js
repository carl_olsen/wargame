'use strict';

var TickAction = require('./base.js');

// used to help animation on front end
var Damage = TickAction
    .compose()
    .props({
        tickActionType: 'damage',
        tickId:         null,
        // Unit
        unitId:       null,
        orderId:      null,
        sourceUnitId: null,
        weaponSlot:   null,
        weaponType:   null,
        damage:       null,
    })
    .methods({
        // already applied by attack
        act:    function() {},
        toJSON: function() {
            return {
                tickActionType: this.tickActionType,
                tickId:         this.tickId,
                unitId:         this.unitId,
                orderId:        this.orderId,
                sourceUnitId:   this.sourceUnitId,
                damage:         this.damage,
                weaponSlot:     this.weaponSlot,
                weaponType:     this.weaponType,
            }
        }
    });

module.exports = Damage;
