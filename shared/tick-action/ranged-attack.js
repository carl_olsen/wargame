'use strict';

var TickAction       = require('./base.js');
var DamageTickAction = require('./damage');

var RangedAttack = TickAction
    .compose()
    .props({
        tickActionType: 'ranged_attack',
        gameState:      null,

        // Unit
        unitId:       null,
        targetUnitId: null,
        weaponSlot:   null,
        weaponType:   null,

        // after act
        damage:              null,
        targetHpAfterDamage: null,

        attackResult: null,
    })
    .methods({
        // apply state changes for server
        act: function() {
            var gameState = this.gameState;
            var source    = gameState.units.get(this.unitId);
            var target    = gameState.units.get(this.targetUnitId);
            var weapon    = source.weapons[this.weaponSlot];
            var damage    = weapon.damage;

            weapon.fire();
            target.damage(damage);
            var damageTickAction = DamageTickAction({
                orderId:      this.orderId,
                tickId:       this.tickId,
                unitId:       this.targetUnitId,
                sourceUnitId: this.unitId,
                weaponSlot:   this.weaponSlot,
                weaponType:   this.weaponType,
                damage:       damage,
                killed:       !target.alive,
            });

            return [this, damageTickAction];
        },
        toJSON: function() {
            return {
                tickActionType: this.tickActionType,
                orderId:        this.orderId,
                tickId:         this.tickId,
                unitId:         this.unitId,
                targetUnitId:   this.targetUnitId,
                weaponSlot:     this.weaponSlot,
                weaponType:     this.weaponType,
            }
        }
    });

module.exports = RangedAttack;
