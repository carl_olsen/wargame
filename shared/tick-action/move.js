'use strict';

var TickAction = require('./base.js');

var Move = TickAction
    .compose()
    .props({
        tickActionType: 'move',
        gameState: null,

        // Unit
        unitId: null,
        // pos
        dest: null,

        orderId: null,
        tickId:  null,

    })
    .methods({
        // apply state changes for server
        act: function() {
            var unit = this.gameState.units.get(this.unitId);
            unit.x = this.dest.x;
            unit.y = this.dest.y;

            return [this];
        },
        toJSON: function() {
            return {
                tickActionType: this.tickActionType,

                unitId:  this.unitId,
                orderId: this.orderId,
                dest:    this.dest,
                tickId:  this.tickId,
            }
        }
    });

module.exports = Move;
