'use strict';

var stampit = require('stampit');

// represnets state change
var TickAction = stampit()
    .props({
        // Unit
        unitId: null,
        gameState: null,
    })
    .methods({
        // apply state change
        // returns array of tickActions, self and any created as a side effect of resolving this tickAction
        // returned tickActions are assumed to have tickAction#.act() called already
        act:    function() {
            return [this];
        },
        toJSON: function() {
            return {
                tickActionType: this.tickActionType,
                orderId:        this.orderId,
                unitId:         this.unitId,
                tickId:         this.tickId,
            }
        }
    });

module.exports = TickAction;
