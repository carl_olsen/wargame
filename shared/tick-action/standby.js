'use strict';

var TickAction = require('./base.js');

var Standby = TickAction
    .compose()
    .props({
        tickActionType: 'standby',
        gameState:      null,
    })
    .methods({

    });

module.exports = Standby;