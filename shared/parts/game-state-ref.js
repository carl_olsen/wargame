'use strict';

var stampit = require('stampit');

var GameStateRef = stampit()

    .refs({
        gameState: null,
    })
    .init(function() {
        if (this.gameState) {
            this.initGameState(this.gameState);
        }
    })
    .methods({
        setGameState: function(gameState) {
            this.gameState = gameState;
            this.initGameState(gameState);
        },
        initGameState: function(gameState) {},
    });


module.exports = GameStateRef;

