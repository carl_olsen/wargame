'use strict';

var stampit = require('stampit');

var Positionable = stampit()
    .props({
        x: 0,
        y: 0,
    })
    .methods({
        pos: function() {
            return {
                x: this.x,
                y: this.y,
            }
        },
        roundedPos: function(){
            return {
                x: parseInt(this.x, 10),
                y: parseInt(this.y, 10),
            }
        },
    });


module.exports = Positionable;

