'use strict';
var Collection   = require('./collection.js');
var Unit         = require('./unit.js');
var _            = require('lodash');
var GameStateRef = require('./parts/game-state-ref.js');

var UnitCollection = Collection
    .props({
    })
    .init(function(s) {
        // var args = s.args;
        // var instance = s.instance;
        // var stamp = s.stamp;
        //
    })
    .methods({
        initGameState: function(gameState) {
            this.each(function(unit) {
                unit.gameState = gameState;
            });
        },
        afterAdd: function(unit) {
            unit.setGameState(this.gameState);
        },
        create: function(settings) {
            var unit = Unit(settings);
            this.add(unit);
            return unit;
        },
        addFromJSON: function(json) {
            var newUnit = Unit.fromJSON(json);
            return this.add(newUnit);
        },
        enemiesOfPlayer: function(playerId, includeDead) {

            return this.filter(function(unit) {

                if(!includeDead && !unit.alive){
                    return false;
                }
                return unit.ownerId !== playerId;
            });
        },
        ownedByPlayer: function(playerId){
            return this.filter(function(unit) {
                return unit.ownerId === playerId;
            });
        },
        toJSON: function() {
            return {
                items: _.values(this.items).map(function(unit) {
                    return unit.toJSON();
                }),
                _idIncrement: this._idIncrement
            };
        }
    })
    .static({
        fromJSON: function(json) {

            var items = json.items;
            json.items = undefined;
            var unitCollection = this(json);

            items.forEach(function(unitJson) {
                unitCollection.addFromJSON(unitJson);
            });

            return unitCollection;
        }
    })
    .compose(GameStateRef);

module.exports = UnitCollection;
