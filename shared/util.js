'use strict';
var getDistance = function(a, b){
    var ad = a.x - b.x
    var bd = a.y - b.y

    return Math.sqrt(ad * ad + bd * bd);
};

var getNormalizedVector = function(a, b, distance){
    distance = distance || getDistance(a, b);
    var vector = {
        x: (b.x - a.x) / distance,
        y: (b.y - a.y) / distance,
    };

    return vector;
};


var rrange = function(min, max) {
    return Math.random() * (max - min) + min;
};

module.exports = {
    getDistance: getDistance,
    getNormalizedVector: getNormalizedVector,
    rrange: rrange,
};