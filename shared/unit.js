'use strict';

var Positionable = require('./parts/positionable');
var OrderStandby = require('./order/standby');
var units        = require('./rules/units.js');
var Weapon       = require('./weapon');
var GameStateRef = require('./parts/game-state-ref.js');

var Unit = Positionable
    .init(function(s) {
        // var args = s.args;
        // var instance = s.instance;
        // var stamp = s.stamp;
        var type = this.unitType;
        var def  = units[type];

        if (!def) {
            throw Error('unit definition not found for unit type: ' + type);
        }
        _.merge(this, def);
        if (this.hp === false) {
            this.hp = this.hpMax;
        }
        var unit = this;

        _.each(this.weapons, function(weapon, key) {
            if (!weapon.toJSON) {
                weapon = Weapon(weapon);
            }
            weapon.setGameState(unit.gameState);
            unit.weapons[key] = weapon;
            weapon.attachToUnit(unit);
        });
    })
    .props({
        // state
        id:      null,
        ownerId: null,
        x:       0,
        y:       0,
        hp:      false,

        // stats from rules
        unitType: 'unit',
        name:     null,
        hpMax:    1,
        speed:    0,
        weapons:  null,
        alive: true,
    })
    .methods({
        initGameState: function(gameState) {

            _.each(this.weapons, function(weapon, key) {
                weapon.setGameState(gameState);
            });
        },

        resetHp: function() {
            this.hp = this.hpMax;
        },
        damage: function(amount) {
            this.hp -= amount;
            if (this.hp <= 0) {
                this.kill();
            }
        },
        setHealth: function(amount) {
            this.hp = amount;
            if (this.hp > this.hpMax) {
                this.hp = this.hpMax;
            }
        },
        heal: function(amount) {
            this.hp += amount;
            if (this.hp > this.hpMax) {
                this.hp = this.hpMax;
            }
        },
        defaultOrder: function() {
            return OrderStandby({
                unitId: this.id
            });
        },
        tick: function(gameState, tickId) {
            _.each(this.weapons, function(weapon) {
                weapon.tick(gameState, tickId);
            });
        },
        weaponMaxRange: function() {
            var weaponRanges = _.map(this.weapons, 'range');
            return _.max(weaponRanges);
        },
        kill: function(){
            this.alive = false;
        },
        toJSON: function() {

            var weapons = _.each(this.weapons, function(weapon, key) {
                weapons[key] = weapon.toJSON();
            });

            return {
                id:       this.id,
                unitType: this.unitType,
                name:     this.name,
                x:        this.x,
                y:        this.y,
                ownerId:  this.ownerId,
                weapons:  weapons,
            }
        }
    })
    .static({
        fromJSON: function(json) {
            _.each(json.weapons, function(weapon, key) {
                weapon.gameState  = json.gameState;
                json.weapons[key] = Weapon.fromJSON(weapon);
            });

            return this(json);
        }
    })
    .compose(GameStateRef)
;

module.exports = Unit;
