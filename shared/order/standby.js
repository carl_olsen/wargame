'use strict';

var Order             = require('./base.js');
var TickActionStandby = require('../tick-action/standby.js');

var Standby = Order
    .compose()
    .props({
        orderType: 'standby'
    })
    .methods({
        execute: function(unit, tickId) {


            var gameState = this.gameState;

            var tickActions = [TickActionStandby({
                gameState: this.gameState,
                tickId:    tickId,
                unitId:    unit.id,
            })];

            var attacks = this.getAttacks(unit, gameState, tickId);
            tickActions = tickActions.concat(attacks);

            return {
                orderComplete: true,
                tickActions:   tickActions,
            };


        },

    });

module.exports = Standby;
