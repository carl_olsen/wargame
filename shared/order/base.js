'use strict';
var stampit                = require('stampit');
var RangedAttackTickAction = require('../tick-action/ranged-attack.js');

var Order = stampit()
    .props({
        gameState: null,
        unitId:    null,
        orderType: 'none',
    })
    .methods({
        execute: function(unit, tickId, orders, ordersIndex) {
            //  return {
            //      complete:   true,
            //      tickActions: tickActions
            //  };
        },
        getAttacks: function(unit, tickId) {
            var gameState = this.gameState;
            var enemies   = gameState.units.enemiesOfPlayer(unit.ownerId);

            var attacks = [];

            _.each(unit.weapons, function(weapon, key) {
                if (!weapon.ready()) {
                    return;
                }
                var targetInfo = weapon.selectTarget(enemies);
                if (targetInfo) {
                    attacks.push(RangedAttackTickAction({
                        unitId:       unit.id,
                        tickId:       tickId,
                        targetUnitId: targetInfo.target.id,
                        weaponType:   weapon.weaponType,
                        gameState:    gameState,
                        weaponSlot:   key,
                    }));
                }
            });
            return attacks;
        },
        toJSON: function() {
            return {
                unitId:    this.unitId,
                orderType: this.orderType,
            }
        }
    })
    .static({
        fromJSON: function(json) {
            return this(json);
        }
    });

module.exports = Order;