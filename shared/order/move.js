'use strict';

var Order = require('./base.js');

var MoveTickAction         = require('../tick-action/move.js');
var RangedAttackTickAction = require('../tick-action/ranged-attack.js');

var rules = require('../rules');

var Move = Order
    .compose()
    .props({
        gameState: null,
        unitId:    null,
        orderType: 'move',
        dest:      {
            x: 0,
            y: 0,
        },
    })
    .methods({
        execute: function(unit, tickId, orders, orderIndex) {
            var gameState = this.gameState;
            var result    = rules.calculateUnitMove({
                map:  gameState.map,
                unit: unit,
                dest: this.dest,
            });

            var complete = result.willPassDestination;

            // cut corner
            if (result.remainingSpeed) {
                var nextOrder = orders[orderIndex + 1];
                if (nextOrder) {
                    var newResult = rules.calculateUnitMove({
                        map:      gameState.map,
                        unit:     unit,
                        dest:     nextOrder.dest,
                        speed:    result.remainingSpeed,
                        startPos: result.pos,
                    });
                    result   = newResult;
                    complete = true;
                }
            }

            var tickAction = MoveTickAction({
                tickId:    tickId,
                unitId:    unit.id,
                dest:      result.pos,
                speed:     result.speed,
                gameState: gameState
            });

            var tickActions = [tickAction];

            var attacks = this.getAttacks(unit, tickId);
            tickActions = tickActions.concat(attacks);

            return {
                orderComplete: complete,
                tickActions:   tickActions,
            };

        },

        toJSON: function() {
            return {
                unitId:    this.unitId,
                orderType: this.orderType,
                dest:      this.dest,
            }
        }
    })
    .static({
        fromJSON: function(json) {
            return this(json);
        }
    });

module.exports = Move;