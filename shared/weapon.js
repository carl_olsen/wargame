'use strict';

var stampit     = require('stampit');
var weapons     = require('./rules/weapons.js');
var getDistance = require('./util').getDistance;
var GameStateRef = require('./parts/game-state-ref.js');

var Weapon = stampit
    .compose()
    .init(function(s) {
        var args = s.args;
        // var instance = s.instance;
        // var stamp = s.stamp;
        var type = this.weaponType;
        var def  = weapons[type];

        if (!def) {
            throw Error('weapon definition not found for unit type: ' + type);
        }
        _.merge(this, def)
    })
    .props({
        weaponType: null,
        name:       null,
        range:      0,
        cooldown:   1,

        // state
        // cooldown ticks left
        unit:              null,
        cooldownRemaining: 0,
    })
    .methods({
        attachToUnit: function(unit) {
            this.unit = unit;
        },
        tick: function(tickId) {
            if (this.cooldownRemaining) {
                this.cooldownRemaining--;
            }
        },
        ready: function() {
            // console.log('ready', this.unit.id, this.cooldownRemaining <= 0);
            return this.cooldownRemaining <= 0;
        },
        fire: function() {
            this.cooldownRemaining = this.cooldown;
        },
        targetInLos: function(targetUnit) {
            // @TODO
            return true;
        },
        targetRange: function(targetUnit) {
            return getDistance(this.unit, targetUnit);
        },
        targetInRange: function(targetUnit, distance) {
            distance = distance || this.targetRange(targetUnit);
            return distance <= this.range;
        },
        targetValid: function(targetUnit) {
            var inRange = this.targetInRange(targetUnit);
            var inLos   = this.targetInLos(targetUnit);
            return inRange && inLos;
        },
        targetInfo: function(targetUnit) {
            var distance = this.targetRange(targetUnit);

            return {
                target:   targetUnit,
                distance: distance,
                inRange:  this.targetInRange(targetUnit, distance),
                inLos:    this.targetInLos(targetUnit),
            };
        },

        selectTarget: function(enemyUnits) {
            var weapon  = this;
            var targets = enemyUnits
                .map(function(enemy) {
                    return weapon.targetInfo(enemy);
                })
                .filter(function(targetInfo) {
                    return targetInfo.inLos && targetInfo.inRange;
                });

            return _.minBy(targets, function(targetInfo) {
                return targetInfo.distance;
            });

        },

        toJSON: function() {
            return {
                weaponType:        this.weaponType,
                name:              this.name,
                cooldownRemaining: this.cooldownRemaining,
            }
        }
    })
    .static({
        fromJSON: function(json) {
            return this(json);
        }
    })
        .compose(GameStateRef)
;

module.exports = Weapon;
