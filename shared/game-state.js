'use strict';
var UnitCollection = require('./unit-collection.js');

var GameState = function(settings) {
    settings = settings || {};

    var out = {
        units:  settings.units,
        map:    settings.map,
        toJSON: function() {
            return {
                units: this.units.toJSON(),
                map:   this.map,
            }
        }
    };
    out.units = out.units || UnitCollection({
            gameState: out
        });
    return out;
};

GameState.fromJSON = function(json) {
    json = json || {};
    var gameState = GameState(json);
    if (json.units) {
        json.gameState = gameState;
        json.units     = UnitCollection.fromJSON(json.units);
    }
    return gameState;
};
module.exports = GameState;
