'use strict';
var _ = require('lodash');

var MapFactory = function MapFactory(settings) {
    var defaults = {
        name: null,
        data: null,
    };

    var s = settings = _.defaults(settings, defaults);

    var methods = {
        getTile: function(x, y) {

            var tx = parseInt(x, 10);
            var ty = parseInt(y, 10);

            if(
                (tx < 0 || this.width < tx) ||
                (ty < 0 || this.height < ty)
            ){
                return false;
            }
            return this.data[ty][tx];
        },
        toJSON: function(){
            return {
                name: this.name,
                data: this.data,
            };
        }
    };

    var props = {
        name:   s.name,
        data:   s.data,
        width:  s.data[0].length,
        height: s.data.length,
    }

    var out = Object.create(methods);
    _.assign(out, props);
    return out;
};

module.exports = MapFactory;