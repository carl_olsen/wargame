'use strict';

var stampit = require('stampit');

var Collection         = stampit()

    .props({
        items: {},
        _idIncrement: 1,
    })
    .methods({
        get: function(id){
            return this.items[id];
        },
        add: function(unit){
            var id = unit.id = unit.id || this._idIncrement++;
            this.items[id] = unit;
            if(this.afterAdd){
                this.afterAdd(unit);
            }
        },
        afterAdd: false,
        remove: function(unit){
            delete this.items[unit.id];
        },
        each: function(func){
            var items = this.items;
            Object.keys(items).forEach(function(id){
                var ent = items[id];
                func(ent, id);
            });
        },
        filter: function(func){
            return _.filter(this.items, func);
        },
        first: function(func){
            var items = this.items;
            var keys = Object.keys(items);

            for(var i = keys.length - 1; i >= 0; i--){
               var key = keys[i];
               var ent = items[key];

               if(func(ent)){
                    return ent;
               }
            }
        }
    });

module.exports = Collection;
